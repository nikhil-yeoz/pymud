"""Area batch file for midgaard."""

#HEADER

from typeclasses.rooms import Room
from typeclasses.exits import Exit
from evennia.utils.create import create_object

#CODE

# Rooms are here.
room_3001 = create_object(Room, key="The Temple Of Mota", aliases=["v3001"])
room_3002 = create_object(Room, key="Priest's Inner Sanctum", aliases=["v3002"])
room_3003 = create_object(Room, key="Priest's Bar", aliases=["v3003"])
room_3004 = create_object(Room, key="Entrance to Priest's Guild", aliases=["v3004"])
room_3005 = create_object(Room, key="The Temple Square", aliases=["v3005"])
room_3006 = create_object(Room, key="Entrance to the Grunting Boar Inn", aliases=["v3006"])
room_3007 = create_object(Room, key="The Grunting Boar", aliases=["v3007"])
room_3008 = create_object(Room, key="The Defunct Reception", aliases=["v3008"])
room_3009 = create_object(Room, key="The Bakery", aliases=["v3009"])
room_3010 = create_object(Room, key="The General Store", aliases=["v3010"])
room_3011 = create_object(Room, key="The Weapon Shop", aliases=["v3011"])
room_3012 = create_object(Room, key="Main Street", aliases=["v3012"])
room_3013 = create_object(Room, key="Main Street", aliases=["v3013"])
room_3014 = create_object(Room, key="Market Square", aliases=["v3014"])
room_3015 = create_object(Room, key="The Main Street", aliases=["v3015"])
room_3016 = create_object(Room, key="The Main Street", aliases=["v3016"])
room_3017 = create_object(Room, key="Entrance to Mage's Guild", aliases=["v3017"])
room_3018 = create_object(Room, key="Mage's Bar", aliases=["v3018"])
room_3019 = create_object(Room, key="Mage's Laboratory", aliases=["v3019"])
room_3020 = create_object(Room, key="The Armoury", aliases=["v3020"])
room_3021 = create_object(Room, key="Entrance Hall to the Guild of Swordsmen", aliases=["v3021"])
room_3022 = create_object(Room, key="The Bar of Swordsmen", aliases=["v3022"])
room_3023 = create_object(Room, key="The Tournament and Practice Yard", aliases=["v3023"])
room_3024 = create_object(Room, key="Eastern End of Poor Alley", aliases=["v3024"])
room_3025 = create_object(Room, key="The Common Square", aliases=["v3025"])
room_3026 = create_object(Room, key="The Dark Alley", aliases=["v3026"])
room_3027 = create_object(Room, key="Entrance Hall to the Guild of Thieves", aliases=["v3027"])
room_3028 = create_object(Room, key="The Thieves Bar", aliases=["v3028"])
room_3029 = create_object(Room, key="The Secret Yard", aliases=["v3029"])
room_3030 = create_object(Room, key="The Dump", aliases=["v3030"])
room_3031 = create_object(Room, key="The Pet Shop", aliases=["v3031"])
room_3032 = create_object(Room, key="Pet Shop Store", aliases=["v3032"])
room_3033 = create_object(Room, key="The Magic Shop", aliases=["v3033"])
room_3034 = create_object(Room, key="The Jeweller's Shop", aliases=["v3034"])
room_3035 = create_object(Room, key="The Leather Shop", aliases=["v3035"])
room_3040 = create_object(Room, key="Inside the West Gate of Midgaard", aliases=["v3040"])
room_3041 = create_object(Room, key="Inside the East Gate of Midgaard", aliases=["v3041"])
room_3042 = create_object(Room, key="Wall Road", aliases=["v3042"])
room_3043 = create_object(Room, key="Wall Road", aliases=["v3043"])
room_3044 = create_object(Room, key="Poor Alley", aliases=["v3044"])
room_3045 = create_object(Room, key="Alley at Levee", aliases=["v3045"])
room_3046 = create_object(Room, key="Eastern end of Alley", aliases=["v3046"])
room_3047 = create_object(Room, key="Wall Road", aliases=["v3047"])
room_3048 = create_object(Room, key="Grubby Inn", aliases=["v3048"])
room_3049 = create_object(Room, key="Levee", aliases=["v3049"])
room_3050 = create_object(Room, key="Abandoned Warehouse", aliases=["v3050"])
room_3051 = create_object(Room, key="On the Bridge", aliases=["v3051"])
room_3052 = create_object(Room, key="Outside the West Gate of Midgaard", aliases=["v3052"])
room_3053 = create_object(Room, key="Outside the East Gate of Midgaard", aliases=["v3053"])
room_3054 = create_object(Room, key="By the Temple Altar", aliases=["v3054"])
room_3055 = create_object(Room, key="Passing into the Mortal Realm", aliases=["v3055"])
room_3056 = create_object(Room, key="The Fountain of Youth Apothecary", aliases=["v3056"])
room_3057 = create_object(Room, key="In the air...", aliases=["v3057"])
room_3058 = create_object(Room, key="Within the Light of Birth", aliases=["v3058"])
room_3059 = create_object(Room, key="Training Room", aliases=["v3059"])
room_3060 = create_object(Room, key="Practice Room", aliases=["v3060"])
room_3062 = create_object(Room, key="The Room of Passage", aliases=["v3062"])
room_3063 = create_object(Room, key="Practice Room", aliases=["v3063"])
room_3092 = create_object(Room, key="", aliases=["v3092"])
room_3093 = create_object(Room, key="", aliases=["v3093"])
room_3094 = create_object(Room, key="", aliases=["v3094"])
room_3095 = create_object(Room, key="", aliases=["v3095"])
room_3096 = create_object(Room, key="", aliases=["v3096"])
room_3097 = create_object(Room, key="A dark tunnel", aliases=["v3097"])
room_3100 = create_object(Room, key="Northwest end of Concourse", aliases=["v3100"])
room_3101 = create_object(Room, key="Promenade", aliases=["v3101"])
room_3102 = create_object(Room, key="Promenade", aliases=["v3102"])
room_3103 = create_object(Room, key="Austral Square", aliases=["v3103"])
room_3104 = create_object(Room, key="Northeast end of Concourse", aliases=["v3104"])
room_3105 = create_object(Room, key="Park Entrance", aliases=["v3105"])
room_3106 = create_object(Room, key="Park Cafe", aliases=["v3106"])
room_3107 = create_object(Room, key="Small path through the park", aliases=["v3107"])
room_3108 = create_object(Room, key="Small path in the park", aliases=["v3108"])
room_3109 = create_object(Room, key="Small path in the park", aliases=["v3109"])
room_3110 = create_object(Room, key="Cityguard Head Quarters", aliases=["v3110"])
room_3111 = create_object(Room, key="Park Road", aliases=["v3111"])
room_3112 = create_object(Room, key="Western Park Entrance", aliases=["v3112"])
room_3113 = create_object(Room, key="A path in the park", aliases=["v3113"])
room_3114 = create_object(Room, key="The Pond", aliases=["v3114"])
room_3115 = create_object(Room, key="A path in the park", aliases=["v3115"])
room_3116 = create_object(Room, key="Eastern Park Entrance", aliases=["v3116"])
room_3117 = create_object(Room, key="Emerald Avenue", aliases=["v3117"])
room_3118 = create_object(Room, key="Park Road", aliases=["v3118"])
room_3119 = create_object(Room, key="Emerald Avenue", aliases=["v3119"])
room_3120 = create_object(Room, key="Road Crossing", aliases=["v3120"])
room_3121 = create_object(Room, key="Emerald Avenue", aliases=["v3121"])
room_3122 = create_object(Room, key="Park Road", aliases=["v3122"])
room_3123 = create_object(Room, key="Elm Street", aliases=["v3123"])
room_3124 = create_object(Room, key="Elm Street", aliases=["v3124"])
room_3125 = create_object(Room, key="Emerald Avenue", aliases=["v3125"])
room_3126 = create_object(Room, key="Park Road", aliases=["v3126"])
room_3127 = create_object(Room, key="On the Concourse", aliases=["v3127"])
room_3128 = create_object(Room, key="On the Concourse", aliases=["v3128"])
room_3129 = create_object(Room, key="On the Concourse", aliases=["v3129"])
room_3130 = create_object(Room, key="On the Concourse", aliases=["v3130"])
room_3131 = create_object(Room, key="Park Road", aliases=["v3131"])
room_3132 = create_object(Room, key="Emerald Avenue", aliases=["v3132"])
room_3133 = create_object(Room, key="Emerald Avenue", aliases=["v3133"])
room_3134 = create_object(Room, key="Emerald Avenue", aliases=["v3134"])
room_3135 = create_object(Room, key="Park Road", aliases=["v3135"])
room_3136 = create_object(Room, key="Park Road", aliases=["v3136"])
room_3137 = create_object(Room, key="The Waiting Room", aliases=["v3137"])
room_3138 = create_object(Room, key="The Mayor's Office", aliases=["v3138"])
room_3139 = create_object(Room, key="Penny Lane", aliases=["v3139"])
room_3140 = create_object(Room, key="Penny Lane", aliases=["v3140"])
room_3141 = create_object(Room, key="The end of Penny Lane", aliases=["v3141"])
room_3142 = create_object(Room, key="Captain's Office", aliases=["v3142"])
room_3143 = create_object(Room, key="The Jail", aliases=["v3143"])
room_3144 = create_object(Room, key="The end of Elm Street", aliases=["v3144"])
room_3150 = create_object(Room, key="Kate's Diner", aliases=["v3150"])
room_3160 = create_object(Room, key="Melancholy's Maps", aliases=["v3160"])
room_3161 = create_object(Room, key="Melancholy's Room", aliases=["v3161"])
room_3170 = create_object(Room, key="Temple Of Stupid", aliases=["v3170"])
room_3200 = create_object(Room, key="Under the Bridge", aliases=["v3200"])
room_3201 = create_object(Room, key="On the River", aliases=["v3201"])
room_3202 = create_object(Room, key="On the River", aliases=["v3202"])
room_3203 = create_object(Room, key="On the River", aliases=["v3203"])
room_3204 = create_object(Room, key="On the River", aliases=["v3204"])
room_3205 = create_object(Room, key="On the River", aliases=["v3205"])
room_3250 = create_object(Room, key="Wall Road", aliases=["v3250"])
room_3251 = create_object(Room, key="Wall Road", aliases=["v3251"])
room_3252 = create_object(Room, key="Wall Road", aliases=["v3252"])
room_3253 = create_object(Room, key="On the Bridge", aliases=["v3253"])
room_3254 = create_object(Room, key="The South Bridge", aliases=["v3254"])
room_3255 = create_object(Room, key="Inside the South Gate of Midgaard", aliases=["v3255"])
room_3256 = create_object(Room, key="Outside the South Gate of Midgaard", aliases=["v3256"])
room_3257 = create_object(Room, key="Wall Road", aliases=["v3257"])
room_3258 = create_object(Room, key="Wall Road", aliases=["v3258"])
room_3259 = create_object(Room, key="Wall Road", aliases=["v3259"])
room_3260 = create_object(Room, key="The Northwest Corner", aliases=["v3260"])
room_3261 = create_object(Room, key="Wall Road", aliases=["v3261"])
room_3262 = create_object(Room, key="Inside the North Gate of Midgaard", aliases=["v3262"])
room_3263 = create_object(Room, key="Wall Road", aliases=["v3263"])
room_3264 = create_object(Room, key="The Northeast Corner", aliases=["v3264"])
room_3265 = create_object(Room, key="Wall Road", aliases=["v3265"])
room_3266 = create_object(Room, key="Wall Road", aliases=["v3266"])
room_3267 = create_object(Room, key="Wall Road", aliases=["v3267"])
room_3268 = create_object(Room, key="Outside the North Gate of Midgaard", aliases=["v3268"])
room_3270 = create_object(Room, key="On the Concourse", aliases=["v3270"])
room_3271 = create_object(Room, key="On the Concourse near the park", aliases=["v3271"])
room_3272 = create_object(Room, key="On the Concourse at Penny Lane", aliases=["v3272"])
room_3273 = create_object(Room, key="On the Concourse at Elm Street", aliases=["v3273"])
room_3301 = create_object(Room, key="Clan Road", aliases=["v3301"])
room_3302 = create_object(Room, key="Clan Road", aliases=["v3302"])
room_3303 = create_object(Room, key="Clan Road", aliases=["v3303"])
room_3314 = create_object(Room, key="Clan Road", aliases=["v3314"])
room_3315 = create_object(Room, key="Clan Road", aliases=["v3315"])
room_3316 = create_object(Room, key="Clan Road", aliases=["v3316"])
room_3333 = create_object(Room, key="Within the Rift", aliases=["v3333"])
room_3334 = create_object(Room, key="A Money Changer's Room", aliases=["v3334"])
room_3352 = create_object(Room, key="Grebe's Tavern", aliases=["v3352"])
room_3355 = create_object(Room, key="Andy's Pub", aliases=["v3355"])
room_3356 = create_object(Room, key="A Room in the Pub", aliases=["v3356"])
room_3357 = create_object(Room, key="The Bar", aliases=["v3357"])
room_3358 = create_object(Room, key="The Bar", aliases=["v3358"])
room_3359 = create_object(Room, key="The Inn", aliases=["v3359"])
room_3360 = create_object(Room, key="A Dark room", aliases=["v3360"])
room_3361 = create_object(Room, key="An Assassins Study", aliases=["v3361"])
room_3362 = create_object(Room, key="Entrance to the Necromancer Guild", aliases=["v3362"])
room_3363 = create_object(Room, key="Necromancer Bar", aliases=["v3363"])
room_3364 = create_object(Room, key="Necromancer Sanctum", aliases=["v3364"])
room_3365 = create_object(Room, key="Entrance to the Bard's Guild", aliases=["v3365"])
room_3366 = create_object(Room, key="The Bard's Guild", aliases=["v3366"])
room_3367 = create_object(Room, key="Entrance to the Anti-Paladin", aliases=["v3367"])
room_3368 = create_object(Room, key="Torture Chamber", aliases=["v3368"])
room_3369 = create_object(Room, key="Paladin Guild", aliases=["v3369"])
room_3370 = create_object(Room, key="Paladin Guild", aliases=["v3370"])
room_3392 = create_object(Room, key="", aliases=["v3392"])
room_3393 = create_object(Room, key="A Mangificently Library", aliases=["v3393"])
room_3394 = create_object(Room, key="A Large Training Room", aliases=["v3394"])
room_3395 = create_object(Room, key="A small room with three doors", aliases=["v3395"])
room_3396 = create_object(Room, key="A dark tunnel", aliases=["v3396"])
room_3397 = create_object(Room, key="A dark tunnel", aliases=["v3397"])
room_3398 = create_object(Room, key="A dark tunnel", aliases=["v3398"])
room_3399 = create_object(Room, key="A dark tunnel", aliases=["v3399"])

# Room descriptions
room_3001.db.desc = """You are in the southern end of the temple hall in the Temple of Mota. The
temple has been constructed from giant marble blocks, eternal in appearance, and
most of the walls are covered by ancient wall paintings picturing gods, giants
and peasants."""
room_3002.db.desc = """This is the inner sanctum. A picture of the mighty Mota is hanging on the wall,
just above the altar which is set against the western wall. A well in the middle
of the floor leads down into darkness. Vile smells waft from the depths."""
room_3003.db.desc = """The bar is one of the finest in the land, lucky it is members only. Fine
furniture is set all around the room. A small sign is hanging on the wall."""
room_3004.db.desc = """The entrance hall is a small modest room, reflecting the true nature of the
Priests. The exit leads east to the temple square. A small entrance to the bar
is in the northern wall."""
room_3005.db.desc = """You are standing on the temple square. Huge marble steps lead up to the temple
gate. The entrance to the Priests Guild is to the west, and the old Grunting
Boar Inn, is to the east. Just south of here you see the market square, the
center of Midgaard."""
room_3006.db.desc = """You are standing in the entrance hall of the Grunting Boar Inn. The hall has
been wisely decorated with simple but functional furniture. The bar is to the
east."""
room_3007.db.desc = """You are standing in the bar. The bar is set against the northern wall, old
archaic writing, carvings and symbols cover its top. A fireplace is built into
the western wall, and through the southeastern windows you can see the temple
square. This place makes you feel like home. A small wooden sign has been posted
near the door."""
room_3008.db.desc = """You are standing in a dusty, defunct reception. The staircase leads down to the
entrance hall."""
room_3009.db.desc = """You are standing inside the small bakery. A sweet scent of Danish and fine
bread fills the room. The bread and Danish are arranged in fine order on the
shelves, and seem to be of the finest quality. A small sign is on the counter."""
room_3010.db.desc = """You are inside the general store. All sorts of items are stacked on shelves
behind the counter, safely out of your reach. A small note hangs on the wall."""
room_3011.db.desc = """You are inside the weapon shop. There is a small note on the counter."""
room_3012.db.desc = """You are at the end of the main street of Midgaard. South of here is the
entrance to the Guild of Magic Users. The street continues east towards the
market square. The magic shop is to the north and to the west is the city gate."""
room_3013.db.desc = """You are on the main street passing through the City of Midgaard. South of here
is the entrance to the Armoury, and the bakery is to the north. East of here is
the market square."""
room_3014.db.desc = """You are standing on the market square, the famous Square of Midgaard. A large,
peculiar looking statue is standing in the middle of the square. Roads lead in
every direction, north to the temple square, south to the common square, east
and westbound is the main street."""
room_3015.db.desc = """You are on Main Street crossing through town. To the north is the general
store, and the main street continues east. To the west you see and hear the
market place and to the south is the Jeweller's Shop."""
room_3016.db.desc = """The main street, to the north is the weapon shop and to the south is the Guild
of Swordsmen. To the east you leave town and to the west the street leads to the
market square."""
room_3017.db.desc = """The entrance hall is a small, poor lighted room."""
room_3018.db.desc = """The bar is one of the wierdest in the land. Mystical images float around the
air. Illusions of fine furniture appear all around the room."""
room_3019.db.desc = """This is the Magical Experiments Laboratory. Dark smoke-stained stones arch over
numerous huge oaken tables, most of these cluttered with strange- looking pipes
and flasks. The floor is covered with half-erased pentagrams and even weirder
symbols, and a blackboard in a dark corner has only been partially cleaned, some
painful-looking letters faintly visible. A well in the middle of the floor leads
down into darkness. Vile smells waft from the depths."""
room_3020.db.desc = """The armoury with all kinds of armours on the walls and in the window. You see
helmets, shields and chain mails. To the north is the main street. On the wall
is a small note."""
room_3021.db.desc = """The entrance hall to the Guild of Swordsmen. A place where one has to be
careful not to say something wrong (or right). To the east is the bar and to the
north is the main street."""
room_3022.db.desc = """The bar of swordsmen, once upon a time beautifully furnished. But now the
furniture is all around you in small pieces. To the south is the yard, and to
the west is the entrance hall."""
room_3023.db.desc = """The practice yard of the fighters. To the north is the bar. A well leads down
into darkness."""
room_3024.db.desc = """You are at the poor alley. South of here is the Grubby Inn and to the east you
see common square. Melancholy's map shop is north. The alley continues further
west."""
room_3025.db.desc = """The common square, people pass you, talking to each other. To the west is the
poor alley and to the east is the dark alley. To the north, this square is
connected to the market square. From the south you notice a nasty smell."""
room_3026.db.desc = """The dark alley, to the west is the common square and to the south is the Guild
of Thieves. Grebe's Tavern, the famous sailor's dive, is north. The alley
continues east."""
room_3027.db.desc = """The entrance hall to the thieves' and assassins' guild. A place where you can
lose both your life and your money, if you are not careful. To the north is the
dark alley and to the east is the thieves' bar."""
room_3028.db.desc = """The bar of the thieves. Once upon a time this place was beautifully furnished,
but now it seems almost empty. To the south is the yard, and to the west is the
entrance hall. (Maybe the furniture has been stolen?!)"""
room_3029.db.desc = """The secret practice yard of thieves and assassins. To the north is the bar. A
well leads down into darkness."""
room_3030.db.desc = """The dump, where the people from the city drop their garbage. Through the
garbage you can see a large junction of pipes, looks like the entrance to the
sewer system. South of here you see the river and to the north is the common
square. A rickety old bridge leads south to the promenade."""
room_3031.db.desc = """The Pet Shop is a small crowded store, full of cages and animals of various
sorts and sizes. There is a sign on the wall."""
room_3032.db.desc = """This is the small dark room in which the Pet Shop Boy keeps his pets. It is
vital that this room be immediately after the pet shop."""
room_3033.db.desc = """You are in a small room that smells of rare chemicals and spices. Dividing the
room in two is a large desk, and on the wall behind it are numerous shelves
crammed with jars, bottles, books and scrolls of all sorts and sizes."""
room_3034.db.desc = """You are in a small, beautifully furnished room. The warm light emanating from
the small oil lamps on the walls is reflected in the hard, polished surface of
the big mahogany desk that stands in the centre of the room. A small sign with
golden letters stands on the desk."""
room_3035.db.desc = """An acrid smell fills this large room. Along the walls are numerous shelves
containing all sorts of animal hide and in the crackling fireplace hangs a big
iron pot with boiling water. In the middle of the room is a large oak table. A
wooden sign is hanging above the fireplace."""
room_3040.db.desc = """You are by two small towers that have been built into the city wall and
connected with a footbridge across the heavy wooden gate. Main Street leads east
and Wall Road leads both north and south from here."""
room_3041.db.desc = """You are by two small towers that have been built into the city wall and
connected with a footbridge across the heavy wooden gate. Main Street leads west
from here. Wall road crosses the street, heading north and south along the city
wall."""
room_3042.db.desc = """You are walking next to the western city wall. The road continues further south
and the city gate is just north of here."""
room_3043.db.desc = """You are walking next to the western city wall. Wall Road continues further
north and south. A small, poor alley leads east. Some letters have been written
on the wall here."""
room_3044.db.desc = """You are in narrow and dirty alley leading east and west. The leather shop is to
the north."""
room_3045.db.desc = """You are standing in the alley which continues east and west. South of here you
see the levee. North is the Pet Shop."""
room_3046.db.desc = """You are standing at the eastern end of the alley, near the city walls. Wall
road loops around the city to your east, and you can see an old three-story
warehouse to the south. The Fountain of Youth Apothecary is north."""
room_3047.db.desc = """You are standing on the road next to the western city wall which continues
north. South of here is a bridge across the river. To the east, you see an odd
looking building painted bright orange with purple trim... It must be the
Practice Room!"""
room_3048.db.desc = """You are inside the old Grubby Inn. This place has not been cleaned for several
decades, vile smells make you dizzy."""
room_3049.db.desc = """You are at the levee. South of here you see the river gently flowing west. The
river bank is very low making it possible to enter the river. Directly across
the river you can see a small boat landing built off of Kate's Diner."""
room_3050.db.desc = """You are inside the only room in the old warehouse. The place is very dusty and
appears to have been unused for many years. The skylight spills violet-tinged
light."""
room_3051.db.desc = """You are standing on the stone bridge crosses the river. The bridge is built out
from the western city wall and the river flows west through an opening in the
wall ten feet below the bridge."""
room_3052.db.desc = """You are by two small towers that have been built into the city wall and
connected with a footbridge across the heavy wooden gate. To the west you can
see the edge of a big forest. A small dusty trail leads north along the wall."""
room_3053.db.desc = """You are by two small towers that have been built into the city wall and
connected with a footbridge across the heavy wooden gate. To the east the plains
stretch out in the distance. To the north a small dusty trail follow the city
wall."""
room_3054.db.desc = """You are by the temple altar in the northern end of the Temple of Mota. A huge
altar made from white polished marble is standing in front of you and behind it
is a ten foot tall sitting statue of Odin, the King of the Gods. The back of the
altar opens into a a beautiful atrium, above which can be seen a rainbow."""
room_3055.db.desc = """You stand within a vast void that has been ripped between the mortal realm and
the realm of birth. Your surroundings pulse lightly in a cyan, magenta, and
black glow decorated by distant and glimmering heavenly beings. The void has
sealed behind you, leaving you only one direction to follow: into the realm of
mortals. {G*{x Newbie weapons can be purchased south and up of the Temple Altar
to the south."""
room_3056.db.desc = """An old man putters around behind the counter, making strange love potions and
curative philtres. He turns as you enter, and looks on with avarice. Judging
from the furnishings, he's been made very wealthy by his trade."""
room_3057.db.desc = """"""
room_3058.db.desc = """Welcome to the fray of Battle. This is the first step to the rest of your life.
KBK is a Player Kill Mud so have fun! {G*{x If you are new here, it is advised
you consult {yhelp{x and {yhelp newbie{x."""
room_3059.db.desc = """The first thing you seem to notice as you walk into the room is that there are
many odd looking objects on the walls. In the center of the room on the floor is
a huge red and black rug, made of the skin of an bear. The room seems to be
lined in gold tremmings, the walls made from redwood. The room smells of sweat
and blood. The celing seems to be made of a buch of strong looking hay, layed
over each other in several thick layers. On the western side of the wall, hangs
a sign that reads \"training room\"."""
room_3060.db.desc = """As you enther the room of learning, you take in your surroundings. On the wall
you notice an picture of a great guildmaster. Along the sides of the wall are
rows of old practice dummies and many old petrufide books of learning that
cannot be opend. This room seems to be made of wood and lined in a golden
border. In the middle of the floor lays a big red, velvet rug with many arcane
runes on it. The runes are that of an wise old man who was the teacher of
everything that there is to know about battle and defense. On the eastern side
of the wall hangs a sign, it reads \"practice room\"."""
room_3062.db.desc = """You have walked into the room of passage. The being in the center of the room
is here to give you brief guidance, to tell you where you need to go. On the
floor lays a big red velvet rug, with fringed ends. The room is made from wood,
lined with a golden trim. There seems to be no celing in this room, it seems to
be open to the elements of the world. The floor around the rug seems to be made
of black dull marble. It looks as if it has been recently build and never walked
upon by a soul."""
room_3063.db.desc = """Here you can practice your skills on a dummy that doesn't hurt very much. New
and improved version two with weapons so you can practice disarm."""
room_3092.db.desc = """"""
room_3093.db.desc = """"""
room_3094.db.desc = """"""
room_3095.db.desc = """"""
room_3096.db.desc = """"""
room_3097.db.desc = """"""
room_3100.db.desc = """You are at the concourse, the city wall is just west. A small promenade goes
east, and the bridge is just north of here. The concourse continues south along
the city wall."""
room_3101.db.desc = """The river gently flows west just north of here. The promenade continues further
east and to the west you see the city wall. Park Road leads south from here."""
room_3102.db.desc = """The river gently flows west just north of here. The promenade continues both
east and west. A rickety bridge crosses the river to the north, towards the city
dump (fortunately, the wind is blowing towards the dump today). South of here
you see the entrance to the park, and a small building seems to be just west of
the entrance. Austral Square, home of Kate's Diner, lies to your east."""
room_3103.db.desc = """The Promenade opens here onto Austral Square, on the south bank of the Midgaard
River. All around you is the bustle of Southern Midgaard-- shopkeepers on break,
a town gossip or two prattling away. You hear the river lapping at the pilings
of the dock by Kate's Diner, which is to your north. Emerald Avenue begins to
the south, the Promenade continues west, and the concourse ends to your east."""
room_3104.db.desc = """You are at the Concourse. The city wall is just east and Austral Square, home
of Kate's Diner, is west. Looking across the river you see an old warehouse. A
strong stone bridge crosses the river to the north, where the concourse becomes
wall road. The Concourse continues south along the wall."""
room_3105.db.desc = """You are standing just inside the small park of Midgaard. To the north is the
promenade and a small path leads south into the park. To your east is the famous
Park Cafe."""
room_3106.db.desc = """You are inside Park Cafe, a very well lighted, cosy place. The cafe is built
from large logs. Through the windows in the northern wall you see the river, and
through the southern window you see many vigorous colorful plants."""
room_3107.db.desc = """You are walking along a small path through the park. The path continues south
and east."""
room_3108.db.desc = """You are standing on a small path inside the park. The park entrance is just
north of here, and Park Cafe is just east of the entrance. The path leads
further east and west."""
room_3109.db.desc = """You are on a small path running through the park. It continues west and south
and just north of here you see the southern wall of Park Cafe."""
room_3110.db.desc = """You are inside a tidy office. A big desk made from dark wood is standing in the
centre of the room."""
room_3111.db.desc = """The road continues north and south. A building is just west of here, you notice
a sign on the door. The park entrance is to the east."""
room_3112.db.desc = """You are standing at the western end of the park. A small path leads east into
the park and going west through the entrance you will reach Park Road."""
room_3113.db.desc = """You are in the park. The paths lead north and west. Westwards is the park
entrance and to the east you see a small pond."""
room_3114.db.desc = """You are swimming around in the pond, feeling rather stupid. You can get back on
the path from the eastern and western end of the pond."""
room_3115.db.desc = """You are in the park. The paths lead north and east. Eastwards is the park
entrance and to the west you see a small pond."""
room_3116.db.desc = """You are standing at the eastern end of the park. A small path leads west into
the park. Going east through the entrance you will reach Emerald Avenue."""
room_3117.db.desc = """You are at Emerald Avenue which continues north and south. To the west is the
park entrance and to the east is the not very big Town Hall of Midgaard."""
room_3118.db.desc = """You are on Park Road which leads north and south."""
room_3119.db.desc = """You are standing at a bend on Emerald Avenue. The road leads north and west. To
the east, you see some run-down slums...it looks like the bad part of town."""
room_3120.db.desc = """You are in the middle of the road cross. Roads lead in all directions. A huge
black iron chain as thick as a tree trunk is fastened into the ground at the
centre of the road cross. Its other end leads directly upwards towards the sky.
A road sign is here."""
room_3121.db.desc = """You are standing at a bend on Emerald Avenue. The road leads south and east."""
room_3122.db.desc = """You are on Park Road which leads south and north. Elm Street is east of here."""
room_3123.db.desc = """You are on Elm street. Park Road is to the west and Elm Street continues in
eastward direction."""
room_3124.db.desc = """You near the end of Elm Street. An old elm tree grows here. A heavy iron
grating is south, surrounding the city's graveyard. You can see a sign hanging
on the grate. The concourse joins Elm Street to the east, and the street ends to
the north near a run-down neighborhood."""
room_3125.db.desc = """You are on Emerald Avenue which continues north. The Concourse is south of
here."""
room_3126.db.desc = """You are on Park Road which continues north. The Concourse is south of here."""
room_3127.db.desc = """You are at the southwest corner of the city wall. The Concourse leads both
north and east."""
room_3128.db.desc = """You are walking around the scenic concourse as it continues its path around the
city of Midgaard. The south gate of the city lies to your east, and the
southwestern corner of the city wall is west. Emerald Avenue stretches out to
the north."""
room_3129.db.desc = """The concourse continues its path around the city. The southern gates of the
city are west of here, and park road lies north. The northeastern corner of the
city wall is east."""
room_3130.db.desc = """You are at the southeast corner of the city wall. The Concourse leads both
north and west."""
room_3131.db.desc = """You are at Park Road which continues north and south."""
room_3132.db.desc = """You are standing on the north end of Emerald Avenue. To the north is Austral
Square and Kate's Diner, to the east is the small street Penny Lane."""
room_3133.db.desc = """You are standing at a bend on Emerald Avenue. To the east the road goes on and
to the south is the Road Crossing."""
room_3134.db.desc = """You are standing at a bend on Emerald Avenue. To the west the road goes on and
to the north is the Road Crossing."""
room_3135.db.desc = """You are at a bend on Park Road. To the north the road goes on and to the east
is the Road Crossing. To the south, the ancient cliffs rise from deep under the
ground, forming a wall."""
room_3136.db.desc = """You are at a bend on Park Road. To the south the road goes on and to the west
is the Road Crossing."""
room_3137.db.desc = """You are standing in the waiting room at the town hall. Wooden chairs stand
along the walls and a long desk is placed in the middle of the room."""
room_3138.db.desc = """You are in the not very big office of the Mayor of Midgaard. A large and
polished but completely empty desk is standing in front of an armchair that
looks so comfortable that it most of all resembles a bed with the head end
raised slightly."""
room_3139.db.desc = """You are on Penny Lane. Emerald Avenue is to the west and Penny Lane continues
in eastward direction."""
room_3140.db.desc = """You are on Penny Lane. The narrow road continues north and west, and the
concourse intersects this street to the east."""
room_3141.db.desc = """You are at the end of Penny Lane. The only exit appears to be south."""
room_3142.db.desc = """You are in the Office of the Captain of the Guard. The Midgaard Coat of Arms is
hanging on the north wall and a heavy steel door is to the south."""
room_3143.db.desc = """You are in a dark and humid jail. The dark stone walls are hard and cold to the
touch. A heavy steel door is to the north."""
room_3144.db.desc = """Elm street ends here, next to a dangerous-looking run down neighborhood. You
can enter the slums to the east, or head towards the graveyard to the south. It
occurs to you that this isn't the happiest section of town."""
room_3150.db.desc = """The smells of baking bread and simmering stew lead you dancing through the
doorway of the diner. No greasy spoons to be seen here-- the booth walls are
covered in casual paisleys; the chairs are carved wood, rescued from garage
sales. Most tables have chess or backgammon boards carved and painted on their
fingertip-smooth tops. The refrain of some musical or other from the 1930's
blends with the dusty golden light and the wonderful smells to put you
completely at ease. A huge picture window looks out over the Midgaard River, and
a doorway leads north onto the dock and the South Bank river landing, while the
main entrance to the south returns you to Austral Square. On most days you will
find Kate here, testing a batch of stew or brewing a special pot of cinnamon
coffee, eerily just in time for your arrival Pull up a chair, perhaps play a
game, and take time out to talk."""
room_3160.db.desc = """As you walk in, a sudden gust of wind enters the shop and sends several scraps
of paper fly around the room. You blink as the dust you've stirred up stings
your eyes, and take a look around the shop. This shop is cluttered with old
pieces of parchment, stacks of books, and odd scraps. A desk with slightly less
clutter than the rest of the shop is in front of you. A scroll case full of
scrolls is behind the desk. A bookcase is to the right of you, and to the left
is a window overlooking the Common Square. A small door, half hidden by a stack
of books, leads to a room in the back of the shop."""
room_3161.db.desc = """A faint smell of incense teases your nose as you enter this room. You step onto
a soft bearskin rug and look around. A simple bed lies against the far wall. At
the foot of it is a sturdy, ironbound chest, most likely full of Melancholy's
trappings from her adventuring days. On a small table under a window to the east
is a vase with a single white rose. Other than that, the room is quite bare."""
room_3170.db.desc = """As you enter the abandoned warehouse your eyes blink violently to adjust to the
almost total darkness. You wonder where the windows are as you step into a mud
puddle in the middle of the floor. As your eyes adjust, you realize that there
are mud puddles all around the room, and this is some weird version of windows.
In a flash of revelation, you realize that you've entered the Temple of Stupid."""
room_3200.db.desc = """The arch under the bridge is covered by seaweed for one foot above the surface
of the river. The water gently flows through an opening in the lower part of the
city wall."""
room_3201.db.desc = """North of here you see the miserable buildings of the poor alley. The river
flows west towards the bridge. The riverbanks are too steep to climb."""
room_3202.db.desc = """North of here you see the dump. The river flows from east to west. The
riverbanks are too steep to climb. A bridge passes overhead, connecting the dump
to the promenade."""
room_3203.db.desc = """The levee is directly north of here, and a small boat landing for Kate's Diner
is to the south. The river flows in an east west direction."""
room_3204.db.desc = """You see the warehouse on the northern riverbank. East of here the river passes
under the eastern bridge and through the city wall. The banks of the levee are
west, in the direction of the current."""
room_3205.db.desc = """The river enters from a hole in the eastern city wall. The hole has been
blocked by several vertically positioned iron bars set into the wall. Overhead
you can see the stone buttresses of the eastern bridge."""
room_3250.db.desc = """Wall road extends north and south along the eastern walls of the city. The east
gate is north of you, south is an intersection with a dark alley. You check to
make sure you still have your wallet."""
room_3251.db.desc = """You are walking along the eastern wall of the city, on a road extending between
the east gate and a bridge to the south. A dark, dirty alley lies west of here.
You step carefully around a pool of liquid... Who knows what it could be in a
place this foul..."""
room_3252.db.desc = """The walls of Midgaard tower above you to the east, keeping the city safe from
its enemies... You hope. An intersection with a dark alley lies north, and a
bridge over the Midgaard river is south. Beyond the bridge wall road turns in
the concourse."""
room_3253.db.desc = """You are crossing a strong stone bridge, which spans the Midgaard river. Below
you, the river flows into an opening in the city wall, sealed with strong metal
bars. North of you is wall road, south of here the concourse loops around the
inside of the city walls."""
room_3254.db.desc = """You are standing on a small, rickety bridge leading from the Midgaard City Dump
to the much more attractive promenade of the southern parts of Midgaard. You can
smell the stench of the dump to the north, the fresher air of the promenade
seems preferable. Below you are the swift-moving currents of the Midgaard river."""
room_3255.db.desc = """Two strong stone towers surround a heavy wooden gate set into the city wall, to
guard against the goblins that lurk in the southern forests. You can exit the
city to the south, if you dare, or walk down the concourse to the east or west."""
room_3256.db.desc = """You are standing outside the strong wooden gate on the southern walls of
Midgaard. Guards eye you suspiciously from the two towers on either side of the
gate, as they stand watch to protect the city from the goblins of this forest.
The trees and goblins of the Miden'nir are south, the gate back to Migaard lies
north."""
room_3257.db.desc = """You are walking along wall road, just north of the west gate of Midgaard. The
road continues along the western wall to the north, towards an intersection with
a busy Clan street."""
room_3258.db.desc = """Wall road intersects with a crowded Clan Road here, filled with shops and
artisans. The street stretches off to the east, towards the far wall of the
city. Wall road continues to the north and south."""
room_3259.db.desc = """You are walking along wall road, which extends along the inside of the western
wall of Midgaard. To the south, you can see an intersection with a crowded Clan
street. The northwestern corner of the city wall lies ahead to the north."""
room_3260.db.desc = """You are standing beneath the northwestern corner of the city wall, on a turn in
wall road. You can head south, towards the west gate, or east, to the north
gate."""
room_3261.db.desc = """The city walls tower over you as walk down wall road, between the northwestern
corner of the wall to the west, and the northern gate of Midgaard to the east."""
room_3262.db.desc = """Two small towers surround a strong wooden gate, connected by a small stone
bridge set with murder holes. This gate is not as heavily guarded as the others,
as the plains outside are safe to travel. You can leave midgaard to the north,
or continues along wall road to the west and east. A teeming clan street is
visible to the south, and beyond that the graceful spires of the Temple of Mota,
but there is no way to reach them from here."""
room_3263.db.desc = """You are walking along wall road, between the northern gate of the city and the
northeastern corner of the city walls. You can see bloodstains on the road...
Better be on guard for thieves."""
room_3264.db.desc = """Wall road turns the corner here, as you pass by the northeastern edge of the
city wall. The north gate lies to your west, and the road south leads to the
eastern gate of Midgaard."""
room_3265.db.desc = """Just south of you is an intersection with a crowded clan street. The eastern
wall of Midgaard is beside you, and to the north you can see the northeast
corner of the city wall."""
room_3266.db.desc = """Here, wall road meets up with a busy clan street heading off towards the west,
and the far wall of the city. You can smell spices, perfume, freshly cured hide,
and other strange scents, all mingle together in one heady aroma. Wall road
continues to the north and south."""
room_3267.db.desc = """You are near the center of wall road, just north of the east gate of the city.
To the north, you can see an intersection with a crowded clan street. The guards
at the gate eye you suspiciously from afar."""
room_3268.db.desc = """You stand outside the north gate of the walled city of Midgaard. A dusty trail
is visibile to the north, it stretches around the walls of the city. Further
north are the wide expanses of the northern plains, leading to the village of
Ofcol. You can return to Midgaard to the south."""
room_3270.db.desc = """The strong stone walls of Midgaard rise up to the west, keeping evil at bay.
The concourse heads towards a bridge to the north, and continues south alongside
a beautiful park."""
room_3271.db.desc = """You are walking down the Concoruse, along the western wall of Midgaard. To the
north, the road passes by a beautiful park, to the south, you can see the
southwest corner of the city wall."""
room_3272.db.desc = """You are on a road leading around the inner walls of Midgaard. To the north, the
concourse heads towards a bridge into northern Midgaard. To the south, you can
see an intersection with Elm Street. The quaint paved streets of Penny Lane lie
west."""
room_3273.db.desc = """The air grows chill as you pass near the Elm Street graveyard. The walls of
Midgaard are to the east, and the concourse continues to the north and south. To
your west, you see Elm Street at the graveyard entrance."""
room_3301.db.desc = """You are standing on the western end of Clan Road. The traffic here is light.
This is a quieter section of Midgaard. You can hear the sound of haggling coming
from the east. To the north is the Paladin Guild and south is the Anti-Paladin
Guild, how ironic."""
room_3302.db.desc = """You are walking along Clan Road. To the south of you is the money changer. Clan
Road continues on to the east and the west and the Assassins guild is to the
north."""
room_3303.db.desc = """The shops on this stretch of Clan road are long-closed and boarded up. Not much
traffic passes through this part, as there is nothing to buy. To the north is
the necromancer guild and south is the druids haven."""
room_3314.db.desc = """Noise and light abound on this part of Clan Road. Clan road continues off to
the east and west."""
room_3315.db.desc = """Clan Road seems to widen here slightly, allowing more traffic to travel through
the area. To the south is another entrance to the busy pub. This door is also
open wide and spilling forth the sounds of happy patrons. Clan Road continues to
the east and west."""
room_3316.db.desc = """You are at the eastern end of Clan Road. To the east of you, the tall wall
around the city looms up before you and casts its shadow on you. Clan Road
continues to the west."""
room_3333.db.desc = """It is impossible to understand your surroundings. The air ebbs and flows with
the essence of magic. The space around you buzzes and explodes in spectrums of
color. Your body feels strangly renewed as you stand within this rift in time."""
room_3334.db.desc = """A few strategically placed lanterns keep this place well-lit in the absence of
any windows. The room is surprisingly well appointed with tapestries hanging on
the walls. You'd wager your life the moneylender's guards are behind them, as
there are none here. The moneylender himself works from behind a large desk, no
doubt holding a fortune in gold and silver. A smallish sign hangs on the wall,
lettered in common."""
room_3352.db.desc = """Dive. That's all you can say about this place; its a dive. You step carefully
over the dead and/or drunk sailors on the floor toward the center of the room.
There's no decorations on the wall; they've all been ripped down in anger or for
use as weapons at one time or another. Looking around, you search vainly for a
clean table to sit at, and then give up."""
room_3355.db.desc = """The busy pub is filled with patrons. People are laughing and singing. A
minstrel plays his lute in another part of the pub, and a few people are even
dancing. Andy, the pubs owner, wanders around and checks to see that the needs
of his customers are met. The bar is to your south. East of you is another
section of the pub. You know that there are rooms to be had somewhere around
here..."""
room_3356.db.desc = """This part of the pub is filled with dancing and singing. A wandering minstrel
is on a chair in a corner with his lute. A busty red headed woman is standing
near him and singing a tale of comedic heroism. Drunk men sit laughing at their
tables and busy bar maids weave through the crowd with full mugs and horns of
ale and mead. Another part of the pub is located to your west. The bar is to the
south. A set of steps occupy in a corner of the room. They lead up to the inn
section of this prosperous establishment."""
room_3357.db.desc = """A long mahogany bar is set in the back of the pub. Mugs line a shelf behind the
bar. Bottles of strong, nasty looking liquids decorate one of the lower shelves.
A large cask rests on a table behind the bar. A strong but friendly-looking
bartender stands next to the cask polishing glasses. A couple of the bar maids
stand in a corner and throw the most inviting glances your way. A sign is on the
wall."""
room_3358.db.desc = """A long mahogany bar is set in the back of the pub. Mugs line a shelf behind the
bar. Bottles of strong, nasty looking liquids decorate one of the lower shelves.
A large cask rests on a table behind the bar. A strong but friendly looking
bartender stands next to the cask polishing glasses. A couple of the bar maids
stand in a corner and throw the most inviting glances your way."""
room_3359.db.desc = """You are standing upstairs from Andy's Pub at Andy's Inn. A few chairs and a
couple of end tables give the room a `homey' atmosphere. A large stone fireplace
burns brightly against one wall and warms the room. A large oak desk is nestled
in one corner of the room. A small man with wire-framed glasses sits behind the
desk working dilligently on his books. He looks up as you enter the room. He
looks at you quizzically. He is probably waiting for you to ask him for a room."""
room_3360.db.desc = """The lights in this room are all very dim and kept so on purpose. Incense burns
softly from a corner table giving the place a smokey atmosphere. Shadows seem to
move against the walls, various Walkers practicing their trade obviously."""
room_3361.db.desc = """Your eyes adjust slowly to the room and you make out a few of the younger
students trying to encompass themselves in darkness. There are bottles of wine
and a few plates of bread sitting on the bar. A couple of ornamental chairs are
placed in the corners and an ancient looking rug lies on the floor. The Master
Assassin stands here, watching you should you fail in your studies."""
room_3362.db.desc = """This alcove is shadowed heavily, the stone seeps some kind of clear fluid that
gives it a wet look. The air smells heavily of embalming fluids. To the north is
the necromancer guild and the south is clan row."""
room_3363.db.desc = """On the northern wall is a bar fully stocked, vats of embalming fluids are also
stored along the east and west walls and a huge shelf runs along the top of the
entire western wall, full of reagents and spell compononts."""
room_3364.db.desc = """The room is dark and smelly, scattered reagents are on the floor. A huge
pentagram is is drawn on the floor for students to use during spells if
necessary."""
room_3365.db.desc = """"""
room_3366.db.desc = """"""
room_3367.db.desc = """The smell is the first thing to hit you, it smells of death and gore. Sweet to
a true Anti-Palaldin nose and horrid to those not of the faith. A small stake is
driven into the earthen ground in one corner with a Paladins head mounted on it,
as a reminder to all what good brings to oneself. To the north is Clan Row and
east is the weapons room."""
room_3368.db.desc = """The soft earth floor crushes softly under your feet and a thick haze hangs
heavy in the air. Weapons of war are leaning against the wall and hung on hooks
for people to spare with. The Master Anti-Paladin stands in a corner with his
knife, ready to kill any student who fails his studies. To the west is the
entrance hall."""
room_3369.db.desc = """The stones of the walls emenate a soft glow that brightens the room ever so
softly. Bowls of incense burn in the corner and a small bar is well stocked with
sacramental wines and bottles of purified water. To the south is clan road and
the Paladin guild proper is to the east."""
room_3370.db.desc = """Smoke from the incense lingers in the air and you feel as tho the gods
themselves are in this room. Banners bearing the various Paladin Family Crests
are hung on the walls with care and a whitebearded guildmaster stands in a
corner, praying silently to his god. To the west is the entrance hall of the
guild."""
room_3392.db.desc = """"""
room_3393.db.desc = """When first walking into this room, you notice a lovely chandelier just floading
in the middle of the Library. It lights the room so elegently, not too bright,
and not too dim. Aisle upon aisles fill the room with thousands of books dealing
in the craft of magic and the arcane. It is clean to the point that it is
spotless. The books are all in pristine order and maintained with the utmost of
care. When you walk down an aisle, you almost feel like it goes on forever
though when you turn back to the main aisle, it's just a miner step back to the
center."""
room_3394.db.desc = """This room is massive with a ceiling reaching nearly 50' tall. There are all
swords of training armor and weapons for those who might wish to partake in
melee combat. Dummies are lined up across the room and a large padded mat across
the ground for practicing ground techniques with minimal injury. The room looks
as if it could fit a hundred of the massive titan that at first seemed large
till you compared the Titan to this room."""
room_3395.db.desc = """"""
room_3396.db.desc = """"""
room_3397.db.desc = """"""
room_3398.db.desc = """"""
room_3399.db.desc = """"""

# Exits
exit_3001_to_3054 = create_object(Exit, key="north", aliases=['n'], location=room_3001, destination=room_3054)
exit_3001_to_3005 = create_object(Exit, key="south", aliases=['s'], location=room_3001, destination=room_3005)
# Could not find room for exit between 3001 and 3700
# Could not find room for exit between 3001 and 28000
exit_3002_to_3003 = create_object(Exit, key="east", aliases=['e'], location=room_3002, destination=room_3003)
exit_3003_to_3004 = create_object(Exit, key="south", aliases=['s'], location=room_3003, destination=room_3004)
exit_3003_to_3002 = create_object(Exit, key="west", aliases=['w'], location=room_3003, destination=room_3002)
exit_3004_to_3003 = create_object(Exit, key="north", aliases=['n'], location=room_3004, destination=room_3003)
exit_3004_to_3005 = create_object(Exit, key="east", aliases=['e'], location=room_3004, destination=room_3005)
exit_3005_to_3001 = create_object(Exit, key="north", aliases=['n'], location=room_3005, destination=room_3001)
exit_3005_to_3006 = create_object(Exit, key="east", aliases=['e'], location=room_3005, destination=room_3006)
exit_3005_to_3014 = create_object(Exit, key="south", aliases=['s'], location=room_3005, destination=room_3014)
exit_3005_to_3004 = create_object(Exit, key="west", aliases=['w'], location=room_3005, destination=room_3004)
# Could not find room for exit between 3005 and 29350
exit_3006_to_3007 = create_object(Exit, key="east", aliases=['e'], location=room_3006, destination=room_3007)
exit_3006_to_3005 = create_object(Exit, key="west", aliases=['w'], location=room_3006, destination=room_3005)
exit_3007_to_3006 = create_object(Exit, key="west", aliases=['w'], location=room_3007, destination=room_3006)
exit_3009_to_3013 = create_object(Exit, key="south", aliases=['s'], location=room_3009, destination=room_3013)
exit_3010_to_3015 = create_object(Exit, key="south", aliases=['s'], location=room_3010, destination=room_3015)
exit_3011_to_3016 = create_object(Exit, key="south", aliases=['s'], location=room_3011, destination=room_3016)
exit_3012_to_3033 = create_object(Exit, key="north", aliases=['n'], location=room_3012, destination=room_3033)
exit_3012_to_3013 = create_object(Exit, key="east", aliases=['e'], location=room_3012, destination=room_3013)
exit_3012_to_3017 = create_object(Exit, key="south", aliases=['s'], location=room_3012, destination=room_3017)
exit_3012_to_3040 = create_object(Exit, key="west", aliases=['w'], location=room_3012, destination=room_3040)
exit_3013_to_3009 = create_object(Exit, key="north", aliases=['n'], location=room_3013, destination=room_3009)
exit_3013_to_3014 = create_object(Exit, key="east", aliases=['e'], location=room_3013, destination=room_3014)
exit_3013_to_3020 = create_object(Exit, key="south", aliases=['s'], location=room_3013, destination=room_3020)
exit_3013_to_3012 = create_object(Exit, key="west", aliases=['w'], location=room_3013, destination=room_3012)
exit_3014_to_3005 = create_object(Exit, key="north", aliases=['n'], location=room_3014, destination=room_3005)
exit_3014_to_3015 = create_object(Exit, key="east", aliases=['e'], location=room_3014, destination=room_3015)
exit_3014_to_3025 = create_object(Exit, key="south", aliases=['s'], location=room_3014, destination=room_3025)
exit_3014_to_3013 = create_object(Exit, key="west", aliases=['w'], location=room_3014, destination=room_3013)
exit_3015_to_3010 = create_object(Exit, key="north", aliases=['n'], location=room_3015, destination=room_3010)
exit_3015_to_3016 = create_object(Exit, key="east", aliases=['e'], location=room_3015, destination=room_3016)
exit_3015_to_3034 = create_object(Exit, key="south", aliases=['s'], location=room_3015, destination=room_3034)
exit_3015_to_3014 = create_object(Exit, key="west", aliases=['w'], location=room_3015, destination=room_3014)
exit_3016_to_3011 = create_object(Exit, key="north", aliases=['n'], location=room_3016, destination=room_3011)
exit_3016_to_3041 = create_object(Exit, key="east", aliases=['e'], location=room_3016, destination=room_3041)
exit_3016_to_3021 = create_object(Exit, key="south", aliases=['s'], location=room_3016, destination=room_3021)
exit_3016_to_3015 = create_object(Exit, key="west", aliases=['w'], location=room_3016, destination=room_3015)
exit_3017_to_3012 = create_object(Exit, key="north", aliases=['n'], location=room_3017, destination=room_3012)
exit_3017_to_3018 = create_object(Exit, key="south", aliases=['s'], location=room_3017, destination=room_3018)
exit_3018_to_3017 = create_object(Exit, key="north", aliases=['n'], location=room_3018, destination=room_3017)
exit_3018_to_3019 = create_object(Exit, key="east", aliases=['e'], location=room_3018, destination=room_3019)
exit_3019_to_3018 = create_object(Exit, key="west", aliases=['w'], location=room_3019, destination=room_3018)
exit_3020_to_3013 = create_object(Exit, key="north", aliases=['n'], location=room_3020, destination=room_3013)
exit_3021_to_3016 = create_object(Exit, key="north", aliases=['n'], location=room_3021, destination=room_3016)
exit_3021_to_3022 = create_object(Exit, key="east", aliases=['e'], location=room_3021, destination=room_3022)
exit_3022_to_3023 = create_object(Exit, key="south", aliases=['s'], location=room_3022, destination=room_3023)
exit_3022_to_3021 = create_object(Exit, key="west", aliases=['w'], location=room_3022, destination=room_3021)
exit_3023_to_3022 = create_object(Exit, key="north", aliases=['n'], location=room_3023, destination=room_3022)
exit_3024_to_3160 = create_object(Exit, key="north", aliases=['n'], location=room_3024, destination=room_3160)
exit_3024_to_3025 = create_object(Exit, key="east", aliases=['e'], location=room_3024, destination=room_3025)
exit_3024_to_3044 = create_object(Exit, key="west", aliases=['w'], location=room_3024, destination=room_3044)
exit_3025_to_3014 = create_object(Exit, key="north", aliases=['n'], location=room_3025, destination=room_3014)
exit_3025_to_3026 = create_object(Exit, key="east", aliases=['e'], location=room_3025, destination=room_3026)
exit_3025_to_3030 = create_object(Exit, key="south", aliases=['s'], location=room_3025, destination=room_3030)
exit_3025_to_3024 = create_object(Exit, key="west", aliases=['w'], location=room_3025, destination=room_3024)
exit_3026_to_3352 = create_object(Exit, key="north", aliases=['n'], location=room_3026, destination=room_3352)
exit_3026_to_3045 = create_object(Exit, key="east", aliases=['e'], location=room_3026, destination=room_3045)
exit_3026_to_3027 = create_object(Exit, key="south", aliases=['s'], location=room_3026, destination=room_3027)
exit_3026_to_3025 = create_object(Exit, key="west", aliases=['w'], location=room_3026, destination=room_3025)
exit_3027_to_3026 = create_object(Exit, key="north", aliases=['n'], location=room_3027, destination=room_3026)
exit_3027_to_3028 = create_object(Exit, key="east", aliases=['e'], location=room_3027, destination=room_3028)
exit_3028_to_3029 = create_object(Exit, key="south", aliases=['s'], location=room_3028, destination=room_3029)
exit_3028_to_3027 = create_object(Exit, key="west", aliases=['w'], location=room_3028, destination=room_3027)
exit_3029_to_3028 = create_object(Exit, key="north", aliases=['n'], location=room_3029, destination=room_3028)
exit_3030_to_3025 = create_object(Exit, key="north", aliases=['n'], location=room_3030, destination=room_3025)
exit_3030_to_3254 = create_object(Exit, key="south", aliases=['s'], location=room_3030, destination=room_3254)
exit_3031_to_3045 = create_object(Exit, key="south", aliases=['s'], location=room_3031, destination=room_3045)
exit_3033_to_3012 = create_object(Exit, key="south", aliases=['s'], location=room_3033, destination=room_3012)
exit_3034_to_3015 = create_object(Exit, key="north", aliases=['n'], location=room_3034, destination=room_3015)
exit_3035_to_3044 = create_object(Exit, key="south", aliases=['s'], location=room_3035, destination=room_3044)
exit_3040_to_3257 = create_object(Exit, key="north", aliases=['n'], location=room_3040, destination=room_3257)
exit_3040_to_3012 = create_object(Exit, key="east", aliases=['e'], location=room_3040, destination=room_3012)
exit_3040_to_3042 = create_object(Exit, key="south", aliases=['s'], location=room_3040, destination=room_3042)
# Could not find room for exit between 3040 and 26100
exit_3041_to_3267 = create_object(Exit, key="north", aliases=['n'], location=room_3041, destination=room_3267)
exit_3041_to_3053 = create_object(Exit, key="east", aliases=['e'], location=room_3041, destination=room_3053)
exit_3041_to_3250 = create_object(Exit, key="south", aliases=['s'], location=room_3041, destination=room_3250)
exit_3041_to_3016 = create_object(Exit, key="west", aliases=['w'], location=room_3041, destination=room_3016)
exit_3042_to_3040 = create_object(Exit, key="north", aliases=['n'], location=room_3042, destination=room_3040)
exit_3042_to_3043 = create_object(Exit, key="south", aliases=['s'], location=room_3042, destination=room_3043)
exit_3043_to_3042 = create_object(Exit, key="north", aliases=['n'], location=room_3043, destination=room_3042)
exit_3043_to_3044 = create_object(Exit, key="east", aliases=['e'], location=room_3043, destination=room_3044)
exit_3043_to_3047 = create_object(Exit, key="south", aliases=['s'], location=room_3043, destination=room_3047)
exit_3044_to_3035 = create_object(Exit, key="north", aliases=['n'], location=room_3044, destination=room_3035)
exit_3044_to_3024 = create_object(Exit, key="east", aliases=['e'], location=room_3044, destination=room_3024)
# Could not find room for exit between 3044 and 8800
exit_3044_to_3043 = create_object(Exit, key="west", aliases=['w'], location=room_3044, destination=room_3043)
exit_3045_to_3031 = create_object(Exit, key="north", aliases=['n'], location=room_3045, destination=room_3031)
exit_3045_to_3046 = create_object(Exit, key="east", aliases=['e'], location=room_3045, destination=room_3046)
exit_3045_to_3049 = create_object(Exit, key="south", aliases=['s'], location=room_3045, destination=room_3049)
exit_3045_to_3026 = create_object(Exit, key="west", aliases=['w'], location=room_3045, destination=room_3026)
exit_3046_to_3056 = create_object(Exit, key="north", aliases=['n'], location=room_3046, destination=room_3056)
exit_3046_to_3251 = create_object(Exit, key="east", aliases=['e'], location=room_3046, destination=room_3251)
exit_3046_to_3050 = create_object(Exit, key="south", aliases=['s'], location=room_3046, destination=room_3050)
exit_3046_to_3045 = create_object(Exit, key="west", aliases=['w'], location=room_3046, destination=room_3045)
exit_3047_to_3043 = create_object(Exit, key="north", aliases=['n'], location=room_3047, destination=room_3043)
exit_3047_to_3063 = create_object(Exit, key="east", aliases=['e'], location=room_3047, destination=room_3063)
exit_3047_to_3051 = create_object(Exit, key="south", aliases=['s'], location=room_3047, destination=room_3051)
exit_3048_to_3024 = create_object(Exit, key="north", aliases=['n'], location=room_3048, destination=room_3024)
exit_3049_to_3045 = create_object(Exit, key="north", aliases=['n'], location=room_3049, destination=room_3045)
exit_3049_to_3203 = create_object(Exit, key="south", aliases=['s'], location=room_3049, destination=room_3203)
exit_3050_to_3046 = create_object(Exit, key="north", aliases=['n'], location=room_3050, destination=room_3046)
exit_3051_to_3047 = create_object(Exit, key="north", aliases=['n'], location=room_3051, destination=room_3047)
exit_3051_to_3100 = create_object(Exit, key="south", aliases=['s'], location=room_3051, destination=room_3100)
exit_3052_to_3040 = create_object(Exit, key="east", aliases=['e'], location=room_3052, destination=room_3040)
# Could not find room for exit between 3053 and 3503
exit_3053_to_3041 = create_object(Exit, key="west", aliases=['w'], location=room_3053, destination=room_3041)
exit_3054_to_3001 = create_object(Exit, key="south", aliases=['s'], location=room_3054, destination=room_3001)
# Could not find room for exit between 3054 and 110
exit_3055_to_3054 = create_object(Exit, key="south", aliases=['s'], location=room_3055, destination=room_3054)
exit_3056_to_3046 = create_object(Exit, key="south", aliases=['s'], location=room_3056, destination=room_3046)
exit_3058_to_3062 = create_object(Exit, key="south", aliases=['s'], location=room_3058, destination=room_3062)
exit_3059_to_3062 = create_object(Exit, key="east", aliases=['e'], location=room_3059, destination=room_3062)
exit_3060_to_3062 = create_object(Exit, key="west", aliases=['w'], location=room_3060, destination=room_3062)
exit_3062_to_3058 = create_object(Exit, key="north", aliases=['n'], location=room_3062, destination=room_3058)
exit_3062_to_3060 = create_object(Exit, key="east", aliases=['e'], location=room_3062, destination=room_3060)
exit_3062_to_3055 = create_object(Exit, key="south", aliases=['s'], location=room_3062, destination=room_3055)
exit_3062_to_3059 = create_object(Exit, key="west", aliases=['w'], location=room_3062, destination=room_3059)
# Could not find room for exit between 3063 and 200
exit_3100_to_3051 = create_object(Exit, key="north", aliases=['n'], location=room_3100, destination=room_3051)
exit_3100_to_3101 = create_object(Exit, key="east", aliases=['e'], location=room_3100, destination=room_3101)
exit_3100_to_3270 = create_object(Exit, key="south", aliases=['s'], location=room_3100, destination=room_3270)
exit_3101_to_3102 = create_object(Exit, key="east", aliases=['e'], location=room_3101, destination=room_3102)
exit_3101_to_3131 = create_object(Exit, key="south", aliases=['s'], location=room_3101, destination=room_3131)
exit_3101_to_3100 = create_object(Exit, key="west", aliases=['w'], location=room_3101, destination=room_3100)
exit_3102_to_3254 = create_object(Exit, key="north", aliases=['n'], location=room_3102, destination=room_3254)
exit_3102_to_3103 = create_object(Exit, key="east", aliases=['e'], location=room_3102, destination=room_3103)
exit_3102_to_3105 = create_object(Exit, key="south", aliases=['s'], location=room_3102, destination=room_3105)
exit_3102_to_3101 = create_object(Exit, key="west", aliases=['w'], location=room_3102, destination=room_3101)
exit_3103_to_3150 = create_object(Exit, key="north", aliases=['n'], location=room_3103, destination=room_3150)
exit_3103_to_3104 = create_object(Exit, key="east", aliases=['e'], location=room_3103, destination=room_3104)
exit_3103_to_3132 = create_object(Exit, key="south", aliases=['s'], location=room_3103, destination=room_3132)
exit_3103_to_3102 = create_object(Exit, key="west", aliases=['w'], location=room_3103, destination=room_3102)
exit_3104_to_3253 = create_object(Exit, key="north", aliases=['n'], location=room_3104, destination=room_3253)
exit_3104_to_3272 = create_object(Exit, key="south", aliases=['s'], location=room_3104, destination=room_3272)
exit_3104_to_3103 = create_object(Exit, key="west", aliases=['w'], location=room_3104, destination=room_3103)
exit_3105_to_3102 = create_object(Exit, key="north", aliases=['n'], location=room_3105, destination=room_3102)
exit_3105_to_3106 = create_object(Exit, key="east", aliases=['e'], location=room_3105, destination=room_3106)
exit_3105_to_3108 = create_object(Exit, key="south", aliases=['s'], location=room_3105, destination=room_3108)
exit_3106_to_3105 = create_object(Exit, key="west", aliases=['w'], location=room_3106, destination=room_3105)
exit_3107_to_3108 = create_object(Exit, key="east", aliases=['e'], location=room_3107, destination=room_3108)
exit_3107_to_3113 = create_object(Exit, key="south", aliases=['s'], location=room_3107, destination=room_3113)
exit_3108_to_3105 = create_object(Exit, key="north", aliases=['n'], location=room_3108, destination=room_3105)
exit_3108_to_3109 = create_object(Exit, key="east", aliases=['e'], location=room_3108, destination=room_3109)
exit_3108_to_3107 = create_object(Exit, key="west", aliases=['w'], location=room_3108, destination=room_3107)
# Could not find room for exit between 3108 and 17958
exit_3109_to_3115 = create_object(Exit, key="south", aliases=['s'], location=room_3109, destination=room_3115)
exit_3109_to_3108 = create_object(Exit, key="west", aliases=['w'], location=room_3109, destination=room_3108)
exit_3110_to_3111 = create_object(Exit, key="east", aliases=['e'], location=room_3110, destination=room_3111)
exit_3110_to_3142 = create_object(Exit, key="west", aliases=['w'], location=room_3110, destination=room_3142)
exit_3111_to_3131 = create_object(Exit, key="north", aliases=['n'], location=room_3111, destination=room_3131)
exit_3111_to_3112 = create_object(Exit, key="east", aliases=['e'], location=room_3111, destination=room_3112)
exit_3111_to_3118 = create_object(Exit, key="south", aliases=['s'], location=room_3111, destination=room_3118)
exit_3111_to_3110 = create_object(Exit, key="west", aliases=['w'], location=room_3111, destination=room_3110)
exit_3112_to_3113 = create_object(Exit, key="east", aliases=['e'], location=room_3112, destination=room_3113)
exit_3112_to_3111 = create_object(Exit, key="west", aliases=['w'], location=room_3112, destination=room_3111)
exit_3113_to_3107 = create_object(Exit, key="north", aliases=['n'], location=room_3113, destination=room_3107)
exit_3113_to_3114 = create_object(Exit, key="east", aliases=['e'], location=room_3113, destination=room_3114)
exit_3113_to_3112 = create_object(Exit, key="west", aliases=['w'], location=room_3113, destination=room_3112)
exit_3114_to_3115 = create_object(Exit, key="east", aliases=['e'], location=room_3114, destination=room_3115)
exit_3114_to_3113 = create_object(Exit, key="west", aliases=['w'], location=room_3114, destination=room_3113)
exit_3115_to_3109 = create_object(Exit, key="north", aliases=['n'], location=room_3115, destination=room_3109)
exit_3115_to_3116 = create_object(Exit, key="east", aliases=['e'], location=room_3115, destination=room_3116)
exit_3115_to_3114 = create_object(Exit, key="west", aliases=['w'], location=room_3115, destination=room_3114)
exit_3116_to_3117 = create_object(Exit, key="east", aliases=['e'], location=room_3116, destination=room_3117)
exit_3116_to_3115 = create_object(Exit, key="west", aliases=['w'], location=room_3116, destination=room_3115)
exit_3117_to_3132 = create_object(Exit, key="north", aliases=['n'], location=room_3117, destination=room_3132)
exit_3117_to_3137 = create_object(Exit, key="east", aliases=['e'], location=room_3117, destination=room_3137)
exit_3117_to_3119 = create_object(Exit, key="south", aliases=['s'], location=room_3117, destination=room_3119)
exit_3117_to_3116 = create_object(Exit, key="west", aliases=['w'], location=room_3117, destination=room_3116)
exit_3118_to_3111 = create_object(Exit, key="north", aliases=['n'], location=room_3118, destination=room_3111)
exit_3118_to_3135 = create_object(Exit, key="south", aliases=['s'], location=room_3118, destination=room_3135)
exit_3119_to_3117 = create_object(Exit, key="north", aliases=['n'], location=room_3119, destination=room_3117)
exit_3119_to_3133 = create_object(Exit, key="west", aliases=['w'], location=room_3119, destination=room_3133)
exit_3120_to_3133 = create_object(Exit, key="north", aliases=['n'], location=room_3120, destination=room_3133)
exit_3120_to_3136 = create_object(Exit, key="east", aliases=['e'], location=room_3120, destination=room_3136)
exit_3120_to_3134 = create_object(Exit, key="south", aliases=['s'], location=room_3120, destination=room_3134)
exit_3120_to_3135 = create_object(Exit, key="west", aliases=['w'], location=room_3120, destination=room_3135)
exit_3121_to_3134 = create_object(Exit, key="east", aliases=['e'], location=room_3121, destination=room_3134)
exit_3121_to_3125 = create_object(Exit, key="south", aliases=['s'], location=room_3121, destination=room_3125)
exit_3122_to_3136 = create_object(Exit, key="north", aliases=['n'], location=room_3122, destination=room_3136)
exit_3122_to_3123 = create_object(Exit, key="east", aliases=['e'], location=room_3122, destination=room_3123)
exit_3122_to_3126 = create_object(Exit, key="south", aliases=['s'], location=room_3122, destination=room_3126)
exit_3123_to_3124 = create_object(Exit, key="east", aliases=['e'], location=room_3123, destination=room_3124)
exit_3123_to_3122 = create_object(Exit, key="west", aliases=['w'], location=room_3123, destination=room_3122)
exit_3124_to_3144 = create_object(Exit, key="north", aliases=['n'], location=room_3124, destination=room_3144)
exit_3124_to_3273 = create_object(Exit, key="east", aliases=['e'], location=room_3124, destination=room_3273)
exit_3124_to_3123 = create_object(Exit, key="west", aliases=['w'], location=room_3124, destination=room_3123)
exit_3125_to_3121 = create_object(Exit, key="north", aliases=['n'], location=room_3125, destination=room_3121)
exit_3125_to_3128 = create_object(Exit, key="south", aliases=['s'], location=room_3125, destination=room_3128)
exit_3126_to_3122 = create_object(Exit, key="north", aliases=['n'], location=room_3126, destination=room_3122)
exit_3126_to_3129 = create_object(Exit, key="south", aliases=['s'], location=room_3126, destination=room_3129)
exit_3127_to_3271 = create_object(Exit, key="north", aliases=['n'], location=room_3127, destination=room_3271)
exit_3127_to_3128 = create_object(Exit, key="east", aliases=['e'], location=room_3127, destination=room_3128)
exit_3128_to_3125 = create_object(Exit, key="north", aliases=['n'], location=room_3128, destination=room_3125)
exit_3128_to_3255 = create_object(Exit, key="east", aliases=['e'], location=room_3128, destination=room_3255)
exit_3128_to_3127 = create_object(Exit, key="west", aliases=['w'], location=room_3128, destination=room_3127)
exit_3129_to_3126 = create_object(Exit, key="north", aliases=['n'], location=room_3129, destination=room_3126)
exit_3129_to_3130 = create_object(Exit, key="east", aliases=['e'], location=room_3129, destination=room_3130)
exit_3129_to_3255 = create_object(Exit, key="west", aliases=['w'], location=room_3129, destination=room_3255)
exit_3130_to_3273 = create_object(Exit, key="north", aliases=['n'], location=room_3130, destination=room_3273)
exit_3130_to_3129 = create_object(Exit, key="west", aliases=['w'], location=room_3130, destination=room_3129)
exit_3131_to_3101 = create_object(Exit, key="north", aliases=['n'], location=room_3131, destination=room_3101)
exit_3131_to_3111 = create_object(Exit, key="south", aliases=['s'], location=room_3131, destination=room_3111)
exit_3132_to_3103 = create_object(Exit, key="north", aliases=['n'], location=room_3132, destination=room_3103)
exit_3132_to_3139 = create_object(Exit, key="east", aliases=['e'], location=room_3132, destination=room_3139)
exit_3132_to_3117 = create_object(Exit, key="south", aliases=['s'], location=room_3132, destination=room_3117)
exit_3133_to_3119 = create_object(Exit, key="east", aliases=['e'], location=room_3133, destination=room_3119)
exit_3133_to_3120 = create_object(Exit, key="south", aliases=['s'], location=room_3133, destination=room_3120)
exit_3134_to_3120 = create_object(Exit, key="north", aliases=['n'], location=room_3134, destination=room_3120)
exit_3134_to_3121 = create_object(Exit, key="west", aliases=['w'], location=room_3134, destination=room_3121)
exit_3135_to_3118 = create_object(Exit, key="north", aliases=['n'], location=room_3135, destination=room_3118)
exit_3135_to_3120 = create_object(Exit, key="east", aliases=['e'], location=room_3135, destination=room_3120)
exit_3136_to_3122 = create_object(Exit, key="south", aliases=['s'], location=room_3136, destination=room_3122)
exit_3136_to_3120 = create_object(Exit, key="west", aliases=['w'], location=room_3136, destination=room_3120)
exit_3137_to_3138 = create_object(Exit, key="east", aliases=['e'], location=room_3137, destination=room_3138)
exit_3137_to_3117 = create_object(Exit, key="west", aliases=['w'], location=room_3137, destination=room_3117)
exit_3138_to_3137 = create_object(Exit, key="west", aliases=['w'], location=room_3138, destination=room_3137)
exit_3139_to_3140 = create_object(Exit, key="east", aliases=['e'], location=room_3139, destination=room_3140)
exit_3139_to_3132 = create_object(Exit, key="west", aliases=['w'], location=room_3139, destination=room_3132)
exit_3140_to_3141 = create_object(Exit, key="north", aliases=['n'], location=room_3140, destination=room_3141)
exit_3140_to_3272 = create_object(Exit, key="east", aliases=['e'], location=room_3140, destination=room_3272)
exit_3140_to_3139 = create_object(Exit, key="west", aliases=['w'], location=room_3140, destination=room_3139)
exit_3141_to_3140 = create_object(Exit, key="south", aliases=['s'], location=room_3141, destination=room_3140)
exit_3142_to_3110 = create_object(Exit, key="east", aliases=['e'], location=room_3142, destination=room_3110)
exit_3142_to_3143 = create_object(Exit, key="south", aliases=['s'], location=room_3142, destination=room_3143)
exit_3143_to_3142 = create_object(Exit, key="north", aliases=['n'], location=room_3143, destination=room_3142)
exit_3144_to_3124 = create_object(Exit, key="south", aliases=['s'], location=room_3144, destination=room_3124)
exit_3150_to_3203 = create_object(Exit, key="north", aliases=['n'], location=room_3150, destination=room_3203)
exit_3150_to_3103 = create_object(Exit, key="south", aliases=['s'], location=room_3150, destination=room_3103)
exit_3160_to_3161 = create_object(Exit, key="north", aliases=['n'], location=room_3160, destination=room_3161)
exit_3160_to_3024 = create_object(Exit, key="south", aliases=['s'], location=room_3160, destination=room_3024)
exit_3161_to_3160 = create_object(Exit, key="south", aliases=['s'], location=room_3161, destination=room_3160)
exit_3200_to_3201 = create_object(Exit, key="east", aliases=['e'], location=room_3200, destination=room_3201)
exit_3201_to_3202 = create_object(Exit, key="east", aliases=['e'], location=room_3201, destination=room_3202)
exit_3201_to_3200 = create_object(Exit, key="west", aliases=['w'], location=room_3201, destination=room_3200)
exit_3202_to_3203 = create_object(Exit, key="east", aliases=['e'], location=room_3202, destination=room_3203)
exit_3202_to_3201 = create_object(Exit, key="west", aliases=['w'], location=room_3202, destination=room_3201)
exit_3203_to_3049 = create_object(Exit, key="north", aliases=['n'], location=room_3203, destination=room_3049)
exit_3203_to_3204 = create_object(Exit, key="east", aliases=['e'], location=room_3203, destination=room_3204)
exit_3203_to_3150 = create_object(Exit, key="south", aliases=['s'], location=room_3203, destination=room_3150)
exit_3203_to_3202 = create_object(Exit, key="west", aliases=['w'], location=room_3203, destination=room_3202)
exit_3204_to_3205 = create_object(Exit, key="east", aliases=['e'], location=room_3204, destination=room_3205)
exit_3204_to_3203 = create_object(Exit, key="west", aliases=['w'], location=room_3204, destination=room_3203)
# Could not find room for exit between 3205 and 5001
exit_3205_to_3204 = create_object(Exit, key="west", aliases=['w'], location=room_3205, destination=room_3204)
exit_3250_to_3041 = create_object(Exit, key="north", aliases=['n'], location=room_3250, destination=room_3041)
exit_3250_to_3251 = create_object(Exit, key="south", aliases=['s'], location=room_3250, destination=room_3251)
exit_3251_to_3250 = create_object(Exit, key="north", aliases=['n'], location=room_3251, destination=room_3250)
exit_3251_to_3252 = create_object(Exit, key="south", aliases=['s'], location=room_3251, destination=room_3252)
exit_3251_to_3046 = create_object(Exit, key="west", aliases=['w'], location=room_3251, destination=room_3046)
exit_3252_to_3251 = create_object(Exit, key="north", aliases=['n'], location=room_3252, destination=room_3251)
exit_3252_to_3253 = create_object(Exit, key="south", aliases=['s'], location=room_3252, destination=room_3253)
exit_3253_to_3252 = create_object(Exit, key="north", aliases=['n'], location=room_3253, destination=room_3252)
exit_3253_to_3104 = create_object(Exit, key="south", aliases=['s'], location=room_3253, destination=room_3104)
exit_3254_to_3030 = create_object(Exit, key="north", aliases=['n'], location=room_3254, destination=room_3030)
exit_3254_to_3102 = create_object(Exit, key="south", aliases=['s'], location=room_3254, destination=room_3102)
exit_3255_to_3129 = create_object(Exit, key="east", aliases=['e'], location=room_3255, destination=room_3129)
exit_3255_to_3256 = create_object(Exit, key="south", aliases=['s'], location=room_3255, destination=room_3256)
exit_3255_to_3128 = create_object(Exit, key="west", aliases=['w'], location=room_3255, destination=room_3128)
exit_3256_to_3255 = create_object(Exit, key="north", aliases=['n'], location=room_3256, destination=room_3255)
# Could not find room for exit between 3256 and 3505
exit_3257_to_3258 = create_object(Exit, key="north", aliases=['n'], location=room_3257, destination=room_3258)
exit_3257_to_3040 = create_object(Exit, key="south", aliases=['s'], location=room_3257, destination=room_3040)
exit_3258_to_3259 = create_object(Exit, key="north", aliases=['n'], location=room_3258, destination=room_3259)
exit_3258_to_3301 = create_object(Exit, key="east", aliases=['e'], location=room_3258, destination=room_3301)
exit_3258_to_3257 = create_object(Exit, key="south", aliases=['s'], location=room_3258, destination=room_3257)
exit_3259_to_3260 = create_object(Exit, key="north", aliases=['n'], location=room_3259, destination=room_3260)
exit_3259_to_3258 = create_object(Exit, key="south", aliases=['s'], location=room_3259, destination=room_3258)
exit_3260_to_3261 = create_object(Exit, key="east", aliases=['e'], location=room_3260, destination=room_3261)
exit_3260_to_3259 = create_object(Exit, key="south", aliases=['s'], location=room_3260, destination=room_3259)
exit_3261_to_3262 = create_object(Exit, key="east", aliases=['e'], location=room_3261, destination=room_3262)
exit_3261_to_3260 = create_object(Exit, key="west", aliases=['w'], location=room_3261, destination=room_3260)
exit_3262_to_3268 = create_object(Exit, key="north", aliases=['n'], location=room_3262, destination=room_3268)
exit_3262_to_3263 = create_object(Exit, key="east", aliases=['e'], location=room_3262, destination=room_3263)
exit_3262_to_3261 = create_object(Exit, key="west", aliases=['w'], location=room_3262, destination=room_3261)
exit_3263_to_3264 = create_object(Exit, key="east", aliases=['e'], location=room_3263, destination=room_3264)
exit_3263_to_3262 = create_object(Exit, key="west", aliases=['w'], location=room_3263, destination=room_3262)
exit_3264_to_3265 = create_object(Exit, key="south", aliases=['s'], location=room_3264, destination=room_3265)
exit_3264_to_3263 = create_object(Exit, key="west", aliases=['w'], location=room_3264, destination=room_3263)
exit_3265_to_3264 = create_object(Exit, key="north", aliases=['n'], location=room_3265, destination=room_3264)
exit_3265_to_3266 = create_object(Exit, key="south", aliases=['s'], location=room_3265, destination=room_3266)
exit_3266_to_3265 = create_object(Exit, key="north", aliases=['n'], location=room_3266, destination=room_3265)
exit_3266_to_3267 = create_object(Exit, key="south", aliases=['s'], location=room_3266, destination=room_3267)
exit_3266_to_3316 = create_object(Exit, key="west", aliases=['w'], location=room_3266, destination=room_3316)
exit_3267_to_3266 = create_object(Exit, key="north", aliases=['n'], location=room_3267, destination=room_3266)
exit_3267_to_3041 = create_object(Exit, key="south", aliases=['s'], location=room_3267, destination=room_3041)
# Could not find room for exit between 3268 and 300
exit_3268_to_3262 = create_object(Exit, key="south", aliases=['s'], location=room_3268, destination=room_3262)
exit_3270_to_3100 = create_object(Exit, key="north", aliases=['n'], location=room_3270, destination=room_3100)
exit_3270_to_3271 = create_object(Exit, key="south", aliases=['s'], location=room_3270, destination=room_3271)
exit_3271_to_3270 = create_object(Exit, key="north", aliases=['n'], location=room_3271, destination=room_3270)
exit_3271_to_3127 = create_object(Exit, key="south", aliases=['s'], location=room_3271, destination=room_3127)
exit_3272_to_3104 = create_object(Exit, key="north", aliases=['n'], location=room_3272, destination=room_3104)
exit_3272_to_3273 = create_object(Exit, key="south", aliases=['s'], location=room_3272, destination=room_3273)
exit_3272_to_3140 = create_object(Exit, key="west", aliases=['w'], location=room_3272, destination=room_3140)
exit_3273_to_3272 = create_object(Exit, key="north", aliases=['n'], location=room_3273, destination=room_3272)
exit_3273_to_3130 = create_object(Exit, key="south", aliases=['s'], location=room_3273, destination=room_3130)
exit_3273_to_3124 = create_object(Exit, key="west", aliases=['w'], location=room_3273, destination=room_3124)
exit_3301_to_3369 = create_object(Exit, key="north", aliases=['n'], location=room_3301, destination=room_3369)
exit_3301_to_3302 = create_object(Exit, key="east", aliases=['e'], location=room_3301, destination=room_3302)
exit_3301_to_3367 = create_object(Exit, key="south", aliases=['s'], location=room_3301, destination=room_3367)
exit_3301_to_3258 = create_object(Exit, key="west", aliases=['w'], location=room_3301, destination=room_3258)
exit_3302_to_3360 = create_object(Exit, key="north", aliases=['n'], location=room_3302, destination=room_3360)
exit_3302_to_3303 = create_object(Exit, key="east", aliases=['e'], location=room_3302, destination=room_3303)
exit_3302_to_3334 = create_object(Exit, key="south", aliases=['s'], location=room_3302, destination=room_3334)
exit_3302_to_3301 = create_object(Exit, key="west", aliases=['w'], location=room_3302, destination=room_3301)
exit_3303_to_3362 = create_object(Exit, key="north", aliases=['n'], location=room_3303, destination=room_3362)
exit_3303_to_3314 = create_object(Exit, key="east", aliases=['e'], location=room_3303, destination=room_3314)
exit_3303_to_3365 = create_object(Exit, key="south", aliases=['s'], location=room_3303, destination=room_3365)
exit_3303_to_3302 = create_object(Exit, key="west", aliases=['w'], location=room_3303, destination=room_3302)
exit_3314_to_3315 = create_object(Exit, key="east", aliases=['e'], location=room_3314, destination=room_3315)
exit_3314_to_3303 = create_object(Exit, key="west", aliases=['w'], location=room_3314, destination=room_3303)
exit_3315_to_3316 = create_object(Exit, key="east", aliases=['e'], location=room_3315, destination=room_3316)
exit_3315_to_3356 = create_object(Exit, key="south", aliases=['s'], location=room_3315, destination=room_3356)
exit_3315_to_3314 = create_object(Exit, key="west", aliases=['w'], location=room_3315, destination=room_3314)
exit_3316_to_3266 = create_object(Exit, key="east", aliases=['e'], location=room_3316, destination=room_3266)
exit_3316_to_3315 = create_object(Exit, key="west", aliases=['w'], location=room_3316, destination=room_3315)
# Could not find room for exit between 3333 and 1450
exit_3334_to_3302 = create_object(Exit, key="north", aliases=['n'], location=room_3334, destination=room_3302)
exit_3352_to_3026 = create_object(Exit, key="south", aliases=['s'], location=room_3352, destination=room_3026)
exit_3355_to_3356 = create_object(Exit, key="east", aliases=['e'], location=room_3355, destination=room_3356)
exit_3355_to_3357 = create_object(Exit, key="south", aliases=['s'], location=room_3355, destination=room_3357)
exit_3356_to_3315 = create_object(Exit, key="north", aliases=['n'], location=room_3356, destination=room_3315)
exit_3356_to_3358 = create_object(Exit, key="south", aliases=['s'], location=room_3356, destination=room_3358)
exit_3356_to_3355 = create_object(Exit, key="west", aliases=['w'], location=room_3356, destination=room_3355)
exit_3356_to_3359 = create_object(Exit, key="up", aliases=['u'], location=room_3356, destination=room_3359)
exit_3357_to_3355 = create_object(Exit, key="north", aliases=['n'], location=room_3357, destination=room_3355)
exit_3358_to_3356 = create_object(Exit, key="north", aliases=['n'], location=room_3358, destination=room_3356)
exit_3359_to_3356 = create_object(Exit, key="down", aliases=['d'], location=room_3359, destination=room_3356)
exit_3360_to_3361 = create_object(Exit, key="east", aliases=['e'], location=room_3360, destination=room_3361)
exit_3360_to_3302 = create_object(Exit, key="south", aliases=['s'], location=room_3360, destination=room_3302)
exit_3361_to_3360 = create_object(Exit, key="west", aliases=['w'], location=room_3361, destination=room_3360)
exit_3362_to_3363 = create_object(Exit, key="north", aliases=['n'], location=room_3362, destination=room_3363)
exit_3362_to_3303 = create_object(Exit, key="south", aliases=['s'], location=room_3362, destination=room_3303)
exit_3363_to_3362 = create_object(Exit, key="south", aliases=['s'], location=room_3363, destination=room_3362)
exit_3363_to_3364 = create_object(Exit, key="west", aliases=['w'], location=room_3363, destination=room_3364)
exit_3364_to_3363 = create_object(Exit, key="east", aliases=['e'], location=room_3364, destination=room_3363)
exit_3365_to_3303 = create_object(Exit, key="north", aliases=['n'], location=room_3365, destination=room_3303)
exit_3365_to_3366 = create_object(Exit, key="east", aliases=['e'], location=room_3365, destination=room_3366)
exit_3366_to_3365 = create_object(Exit, key="west", aliases=['w'], location=room_3366, destination=room_3365)
exit_3367_to_3301 = create_object(Exit, key="north", aliases=['n'], location=room_3367, destination=room_3301)
exit_3367_to_3368 = create_object(Exit, key="east", aliases=['e'], location=room_3367, destination=room_3368)
exit_3368_to_3367 = create_object(Exit, key="west", aliases=['w'], location=room_3368, destination=room_3367)
exit_3369_to_3370 = create_object(Exit, key="east", aliases=['e'], location=room_3369, destination=room_3370)
exit_3369_to_3301 = create_object(Exit, key="south", aliases=['s'], location=room_3369, destination=room_3301)
exit_3370_to_3369 = create_object(Exit, key="west", aliases=['w'], location=room_3370, destination=room_3369)
exit_3392_to_3395 = create_object(Exit, key="west", aliases=['w'], location=room_3392, destination=room_3395)
# Could not find room for exit between 3393 and 500
exit_3394_to_3395 = create_object(Exit, key="south", aliases=['s'], location=room_3394, destination=room_3395)
exit_3395_to_3394 = create_object(Exit, key="north", aliases=['n'], location=room_3395, destination=room_3394)
exit_3395_to_3392 = create_object(Exit, key="east", aliases=['e'], location=room_3395, destination=room_3392)
exit_3395_to_3393 = create_object(Exit, key="west", aliases=['w'], location=room_3395, destination=room_3393)
exit_3395_to_3396 = create_object(Exit, key="down", aliases=['d'], location=room_3395, destination=room_3396)
exit_3396_to_3397 = create_object(Exit, key="south", aliases=['s'], location=room_3396, destination=room_3397)
exit_3396_to_3395 = create_object(Exit, key="up", aliases=['u'], location=room_3396, destination=room_3395)
exit_3397_to_3396 = create_object(Exit, key="north", aliases=['n'], location=room_3397, destination=room_3396)
exit_3397_to_3398 = create_object(Exit, key="south", aliases=['s'], location=room_3397, destination=room_3398)
exit_3398_to_3397 = create_object(Exit, key="north", aliases=['n'], location=room_3398, destination=room_3397)
exit_3398_to_3399 = create_object(Exit, key="south", aliases=['s'], location=room_3398, destination=room_3399)
exit_3399_to_3398 = create_object(Exit, key="north", aliases=['n'], location=room_3399, destination=room_3398)
exit_3399_to_3054 = create_object(Exit, key="up", aliases=['u'], location=room_3399, destination=room_3054)

# Exit descriptions
exit_3001_to_3054.db.desc = "At the northern end of the temple hall is a statue and a huge altar."
exit_3001_to_3005.db.desc = "You see the temple square."
# Could not find room for exit between 3001 and 3700
exit_3002_to_3003.db.desc = "You see your favorite place, the bar of divination."
exit_3003_to_3004.db.desc = "You see the entrance."
exit_3003_to_3002.db.desc = "You see the inner sanctum."
exit_3004_to_3003.db.desc = "You see the bar, richly decorated with really stylish furniture."
exit_3004_to_3005.db.desc = "You see the Temple Square."
exit_3005_to_3001.db.desc = "You see the temple."
exit_3005_to_3006.db.desc = "You see the good old Grunting Boar Inn."
exit_3005_to_3014.db.desc = "You see the Market Square."
exit_3005_to_3004.db.desc = "You see the entrance to the Priests Guild."
# Could not find room for exit between 3005 and 29350
exit_3006_to_3007.db.desc = "Surprise! You see the bar."
exit_3006_to_3005.db.desc = "You see the temple square."
exit_3007_to_3006.db.desc = "You see the exit to the entrance hall."
exit_3009_to_3013.db.desc = "You see the main street."
exit_3010_to_3015.db.desc = "You see the main street."
exit_3011_to_3016.db.desc = "You see the main street."
exit_3012_to_3033.db.desc = "You see the magic shop."
exit_3012_to_3013.db.desc = "You see the main street."
exit_3012_to_3017.db.desc = "You see the entrance to the Guild of Magic Users."
exit_3012_to_3040.db.desc = "You see the city gate."
exit_3013_to_3009.db.desc = "You see the bakery."
exit_3013_to_3014.db.desc = "You see the market square."
exit_3013_to_3020.db.desc = "You see the armoury."
exit_3013_to_3012.db.desc = "You see the main street."
exit_3014_to_3005.db.desc = "You see the temple square."
exit_3014_to_3015.db.desc = "You see the main street."
exit_3014_to_3025.db.desc = "You see the common square."
exit_3014_to_3013.db.desc = "You see the main street."
exit_3015_to_3010.db.desc = "You see the general store."
exit_3015_to_3016.db.desc = "You see Main Street."
exit_3015_to_3034.db.desc = "You see the Jeweller's Shop."
exit_3015_to_3014.db.desc = "You see the market square."
exit_3016_to_3011.db.desc = "You see the weapon shop."
exit_3016_to_3041.db.desc = "You see the city gate."
exit_3016_to_3021.db.desc = "You see the swordsmen's guild."
exit_3016_to_3015.db.desc = "You see the main street leading to the market square."
exit_3017_to_3012.db.desc = "You see the main street."
exit_3017_to_3018.db.desc = "You see your favorite place, the Mage's Bar."
exit_3018_to_3017.db.desc = "You see the lobby."
exit_3018_to_3019.db.desc = "You see the laboratory."
exit_3019_to_3018.db.desc = "You see the bar."
exit_3020_to_3013.db.desc = "You see the main street."
exit_3021_to_3016.db.desc = "You see the main street."
exit_3021_to_3022.db.desc = "You see the swordsmen's bar, many noises comes from there."
exit_3022_to_3023.db.desc = "You see the practice yard."
exit_3022_to_3021.db.desc = "You see the entrance hall to the thieves guild."
exit_3023_to_3022.db.desc = "You see the bar."
exit_3024_to_3160.db.desc = "You see Melancholy's Map Shop"
exit_3024_to_3025.db.desc = "You see the common square."
exit_3024_to_3044.db.desc = "You see the poor alley."
exit_3025_to_3014.db.desc = "You see the market square."
exit_3025_to_3026.db.desc = "You see the dark alley."
exit_3025_to_3030.db.desc = "You see the city dump."
exit_3025_to_3024.db.desc = "You see the poor alley."
exit_3026_to_3352.db.desc = "You see a foul-smelling sailor's dive."
exit_3026_to_3045.db.desc = "The alley continues east."
exit_3026_to_3027.db.desc = "You see the entrance to the thieves guild."
exit_3026_to_3025.db.desc = "You see the common square."
exit_3027_to_3026.db.desc = "You see the alley."
exit_3027_to_3028.db.desc = "You see the thieves bar, where everything disappears."
exit_3028_to_3029.db.desc = "You see the secret yard."
exit_3028_to_3027.db.desc = "You see the entrance hall to the thieves guild."
exit_3029_to_3028.db.desc = "You see the bar."
exit_3030_to_3025.db.desc = "You see the common square."
exit_3030_to_3254.db.desc = "You see a river between northern and southern Midgaard. A small bridge"
exit_3031_to_3045.db.desc = "You see the Alley."
exit_3033_to_3012.db.desc = "You see the main street."
exit_3034_to_3015.db.desc = "You see Main Street."
exit_3035_to_3044.db.desc = "You see the alley."
exit_3040_to_3257.db.desc = "Wall road runs along the inner city wall to the north."
exit_3040_to_3012.db.desc = "You see Main Street."
exit_3040_to_3042.db.desc = "You see the road running along the inner side of the city wall. You notice"
exit_3041_to_3267.db.desc = "Wall road heads north along the city wall."
exit_3041_to_3053.db.desc = "You see the city gate."
exit_3041_to_3250.db.desc = "Wall road heads south towards the poor section of town."
exit_3041_to_3016.db.desc = "You see Main Street.~"
exit_3042_to_3040.db.desc = "You see the city gate."
exit_3042_to_3043.db.desc = "The road continues further south."
exit_3043_to_3042.db.desc = "The road continues further north."
exit_3043_to_3044.db.desc = "The alley leads east."
exit_3043_to_3047.db.desc = "The road continues further south."
exit_3044_to_3035.db.desc = "The leather shop is to the north."
exit_3044_to_3024.db.desc = "The alley leads east."
exit_3044_to_3043.db.desc = "The alley leads west."
exit_3045_to_3031.db.desc = "You see, hear, and smell the pet shop."
exit_3045_to_3046.db.desc = "The alley leads east."
exit_3045_to_3049.db.desc = "You see the levee."
exit_3045_to_3026.db.desc = "The alley leads west."
exit_3046_to_3056.db.desc = "You see a pricey-looking potion shop."
exit_3046_to_3251.db.desc = "You see wall road, running a north-south loop around the city."
exit_3046_to_3050.db.desc = "You see the warehouse."
exit_3046_to_3045.db.desc = "You see the alley."
exit_3047_to_3043.db.desc = "You see the road."
exit_3047_to_3051.db.desc = "You see the bridge."
exit_3048_to_3024.db.desc = "You see the alley."
exit_3049_to_3045.db.desc = "You see the Alley."
exit_3049_to_3203.db.desc = "You see the river flowing west."
exit_3050_to_3046.db.desc = "You see the alley."
exit_3051_to_3047.db.desc = "You see the road."
exit_3051_to_3100.db.desc = "You see the Concourse."
exit_3052_to_3040.db.desc = "The city gate is to the east."
# Could not find room for exit between 3053 and 3503
exit_3053_to_3041.db.desc = "You see the city gate."
exit_3054_to_3001.db.desc = "You see the southern end of the temple."
# Could not find room for exit between 3054 and 110
exit_3056_to_3046.db.desc = "You see the eastern end of alley."
# Could not find room for exit between 3063 and 200
exit_3100_to_3051.db.desc = "You see the Bridge."
exit_3100_to_3101.db.desc = "You see the promenade."
exit_3100_to_3270.db.desc = "The concourse continues south."
exit_3101_to_3102.db.desc = "The promenade."
exit_3101_to_3131.db.desc = "Park Road leads south."
exit_3101_to_3100.db.desc = "You see the Concourse."
exit_3102_to_3254.db.desc = "A rickety old bridge crosses the river, heading towards the city dump."
exit_3102_to_3103.db.desc = "You see people moving in and out of Kate's Diner."
exit_3102_to_3105.db.desc = "You see the park entrance."
exit_3102_to_3101.db.desc = "You see the promenade."
exit_3103_to_3150.db.desc = "You smell the heavenly scents of Kate's Diner."
exit_3103_to_3104.db.desc = "You see the Concourse."
exit_3103_to_3132.db.desc = "The small path leads south."
exit_3103_to_3102.db.desc = "You see the promenade."
exit_3104_to_3253.db.desc = "A strong stone bridge crosses the river."
exit_3104_to_3272.db.desc = "The Concourse continues south, towards an intersection with Penny Lane."
exit_3104_to_3103.db.desc = "You Austral Square."
exit_3105_to_3102.db.desc = "You see the promenade."
exit_3105_to_3106.db.desc = "You see Park Cafe."
exit_3105_to_3108.db.desc = "You see the park."
exit_3106_to_3105.db.desc = "You see the park entrance."
exit_3108_to_3105.db.desc = "You see the northern park entrance."
exit_3110_to_3111.db.desc = "You see Park Road."
exit_3110_to_3142.db.desc = "You see a sign saying 'KEEP OUT'."
exit_3111_to_3112.db.desc = "You see the park entrance."
exit_3111_to_3110.db.desc = "You see the cityguard head quarters."
exit_3113_to_3114.db.desc = "You see the pond."
exit_3113_to_3112.db.desc = "You see the western park entrance."
exit_3124_to_3144.db.desc = "You see the beginnings of Midgaard's bad neighborhoods."
exit_3124_to_3273.db.desc = "You see the concourse."
exit_3128_to_3125.db.desc = "Emerald avenue lies to the north."
exit_3128_to_3255.db.desc = "You can see the heavily guarded southern gate of Midgaard."
exit_3129_to_3255.db.desc = "You see the south gate of Midgaard."
exit_3137_to_3138.db.desc = "It looks like some kind of office."
exit_3137_to_3117.db.desc = "The exit west leads to Emerald Avenue."
exit_3138_to_3137.db.desc = "The waiting room is to the west."
exit_3140_to_3272.db.desc = "You see the concourse."
exit_3142_to_3110.db.desc = "You see the Cityguard Head Quarters."
exit_3142_to_3143.db.desc = "You see the heavy steel door."
exit_3143_to_3142.db.desc = "You see the heavy steel door."
exit_3144_to_3124.db.desc = "Elm street continues towards a graveyard."
exit_3150_to_3203.db.desc = "A small boat landing lets boats land at the diner."
exit_3150_to_3103.db.desc = "You see Austral Square."
exit_3160_to_3161.db.desc = "A sign on the door says, \"Unauthorized Personel Prohibited\"."
exit_3160_to_3024.db.desc = "To the south is the Poor Alley. Across the alley, you see the Grubby Inn."
exit_3161_to_3160.db.desc = "A small door to the south leads out into the map shop."
exit_3203_to_3150.db.desc = "You see the landing in the back of Kate's Diner."
# Could not find room for exit between 3205 and 5001
exit_3250_to_3041.db.desc = "The eastern gate of the city is north."
exit_3250_to_3251.db.desc = "You see an intersection with a dark alley."
exit_3251_to_3250.db.desc = "Wall road continues to the north, heading towards the gate."
exit_3251_to_3252.db.desc = "Wall road continues to the south, towards a bridge over the Midgaard river."
exit_3251_to_3046.db.desc = "A dirty, unlight alley lies to the west...it looks like a perfect spot for a"
exit_3252_to_3251.db.desc = "You see an intersection with a dark alley."
exit_3252_to_3253.db.desc = "The eastern bridge crosses the river to the south."
exit_3253_to_3252.db.desc = "Wall road continues to the north."
exit_3253_to_3104.db.desc = "The concourse stretches around the southern parts of Midgaard."
exit_3254_to_3030.db.desc = "You can see the clutter of the City Dump."
exit_3254_to_3102.db.desc = "The beautiful promenade lies along the river. What a romantic place to visit!"
exit_3255_to_3129.db.desc = "The concourse heads east along the inside of the city wall."
exit_3255_to_3256.db.desc = "You see the southern gate."
exit_3255_to_3128.db.desc = "The concourse heads west along the inside of the city wall."
exit_3256_to_3255.db.desc = "You see the southern gate."
# Could not find room for exit between 3256 and 3505
exit_3257_to_3258.db.desc = "You see an intersection between wall road and a busy Clan street."
exit_3257_to_3040.db.desc = "The west gate of the city lies south."
exit_3258_to_3259.db.desc = "Wall road continues to the north, towards the northwestern corner of the wall."
exit_3258_to_3301.db.desc = "You see the shops and clan halls of Clan Road."
exit_3258_to_3257.db.desc = "Wall road continues alongside the western wall, heading towards the west gate."
exit_3259_to_3260.db.desc = "You see the northwestern corner of the city wall."
exit_3259_to_3258.db.desc = "To the south, wall road intersects with a crowded Clan street."
exit_3260_to_3261.db.desc = "Wall road continues along the northern wall of the city."
exit_3260_to_3259.db.desc = "Wall road continues along the western wall of the city."
exit_3261_to_3262.db.desc = "Wall road continues to the east, towards the northern gate of Midgaard."
exit_3261_to_3260.db.desc = "You can see the northwest corner of the city wall to the west."
exit_3262_to_3268.db.desc = "The northern plains are visible through the gate."
exit_3262_to_3263.db.desc = "Wall road continues to the east, alongside the northern wall."
exit_3262_to_3261.db.desc = "Wall road continues to the west, alongside the northern wall."
exit_3263_to_3264.db.desc = "You see the northeastern corner of the city walls."
exit_3263_to_3262.db.desc = "The northern gate of the city lies east."
exit_3264_to_3265.db.desc = "Wall road continues to the south, along the eastern wall of the city."
exit_3264_to_3263.db.desc = "Wall road continues to the west, along the northern wall of the city."
exit_3265_to_3264.db.desc = "You can see the northeast corner of the city wall."
exit_3265_to_3266.db.desc = "You see an intersection with a crowded street."
exit_3266_to_3265.db.desc = "Wall road continues to the north, along the eastern wall of the city."
exit_3266_to_3267.db.desc = "Wall road continues to the south, towards the east gate of Midgaard."
exit_3266_to_3316.db.desc = "You see a mass of people heading down Clan Road."
exit_3267_to_3266.db.desc = "You see an intersection with a busy clan street."
exit_3267_to_3041.db.desc = "You see the well-guarded east gate of Midgaard."
exit_3268_to_3262.db.desc = "You see the north gate of Midgaard."
exit_3270_to_3100.db.desc = "The concourse continues to the north, along the city wall."
exit_3270_to_3271.db.desc = "The concourse heads south, passing by a beautiful park."
exit_3271_to_3270.db.desc = "The concourse heads north, passing by a beautiful park."
exit_3271_to_3127.db.desc = "You can see the southwest corner of the city wall."
exit_3272_to_3104.db.desc = "The concourse continues to the north, towards the east bridge."
exit_3272_to_3273.db.desc = "The concourse continues south, towards an intersection with Elm Street."
exit_3272_to_3140.db.desc = "You see Penny Lane."
exit_3273_to_3272.db.desc = "The concourse continues to the north, towards an intersection with Penny Lane."
exit_3273_to_3130.db.desc = "The concourse continues to the south, towards the southeast corner of the wall."
exit_3273_to_3124.db.desc = "You see Elm Street and the graveyard entrance."
exit_3301_to_3302.db.desc = "Clan Road continues east."
exit_3301_to_3258.db.desc = "Wall Road is to the west."
exit_3302_to_3303.db.desc = "Clan Road continues east."
exit_3302_to_3334.db.desc = "The money changer is to the south."
exit_3302_to_3301.db.desc = "Clan Road continues west."
exit_3303_to_3314.db.desc = "Clan Road continues to the east."
exit_3303_to_3302.db.desc = "Clan Road continues to the west."
exit_3314_to_3315.db.desc = "Clan Road continues to the east."
exit_3314_to_3303.db.desc = "Clan Road continues to the west."
exit_3315_to_3316.db.desc = "Clan Road continues to the east."
exit_3315_to_3356.db.desc = "A busy pub is to the south."
exit_3315_to_3314.db.desc = "Clan Road continues to the west."
exit_3316_to_3266.db.desc = "Wall Road is east of here."
exit_3316_to_3315.db.desc = "Clan Road continues to the west."
exit_3334_to_3302.db.desc = "Clan Road is to the north."
exit_3352_to_3026.db.desc = "A dark alley is to the south."
exit_3355_to_3356.db.desc = "You see another room in the pub."
exit_3355_to_3357.db.desc = "The bar is to the south of you."
exit_3356_to_3315.db.desc = "Clan Road is to the north."
exit_3356_to_3358.db.desc = "The bar is to your south."
exit_3356_to_3355.db.desc = "Another room in the pub is to your west."
exit_3356_to_3359.db.desc = "A set of stairs leads up to the reception area."
exit_3357_to_3355.db.desc = "The pub proper is to the north."
exit_3358_to_3356.db.desc = "The pub proper is to the north."
exit_3359_to_3356.db.desc = "The stairs lead back down to the pub."
exit_3360_to_3302.db.desc = "The money changer is to the south."
# Could not find room for exit between 3393 and 500 