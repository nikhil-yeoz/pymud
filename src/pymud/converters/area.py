from typing import List, Dict

def parse_area_data(areadata: List[str]) -> Dict[str, List[str]]:
    area_info = {}
    for line in areadata:
        if line == 'End':
            break
        squig_stripped = line.strip('~')
        split_line = squig_stripped.split()
        key = split_line[0]
        val = ' '.join(split_line[1:])
        area_info[key] = val
    return area_info
