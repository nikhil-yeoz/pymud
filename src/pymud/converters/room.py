from collections import defaultdict

EXIT_DIRECTIONS = {
    '0': ['north', ['n']],
    '1': ['east', ['e']],
    '2': ['south', ['s']],
    '3': ['west', ['w']],
    '4': ['up', ['u']],
    '5': ['down', ['d']],
}

ROOM_KEYS = ('NAME', 'DESCR', 'FLAGS', 'Sect ', 'EDESC', 'DOOR', 'MHRATE')


def _get_rooms(room_lines):
    rooms = defaultdict(list)
    for line in room_lines:
        if line.startswith('#'):
            room_num = line.strip('#')
            continue
        rooms[room_num].append(line)
    rooms = dict(rooms)
    return rooms.items()


def _get_room_dict(room_lines):
    room = defaultdict(list)
    curr_key = 'VNUM'
    for line in room_lines:
        if line == 'End':
            break
        for key in ROOM_KEYS:
            if key in line:
                curr_key = key
        room[curr_key].append(line)
    return dict(room)


def _clean_doors(door_lines):
    door_dict = defaultdict(list)
    doornum = None
    for line in door_lines:
        if line == '~':
            continue
        if 'DOOR' in line:
            doornum = line.split()[-1]
        if doornum is not None:
            door_dict[doornum].append(line)
    return dict(door_dict)


def _parse_cleaned_doors(cleaned_doors):
    doors = {}
    for direction, door_lines in cleaned_doors.items():
        if len(door_lines) == 3:
            desc = door_lines[1]
        elif len(door_lines) == 2:
            continue
        else:
            desc = ' '.join(door_lines[1:-1])
        exit = door_lines[-1].split()[-1]
        direction, aliases = EXIT_DIRECTIONS[direction]
        doors[direction] = {'description': desc,
                            'exit': exit, 'aliases': aliases}
    return doors


def _parse_doors(door_lines):
    cleaned_doors = _clean_doors(door_lines)
    parsed_doors = _parse_cleaned_doors(cleaned_doors)
    return parsed_doors


def _get_room_info(room_dict):
    room_info = {}
    for key, vals in room_dict.items():
        if key == 'EDESC':
            room_info[key] = _parse_edesc(vals)
        elif key == 'DOOR':
            room_info[key] = _parse_doors(vals)
        else:
            room_info[key] = '\n'.join(vals).strip(key).strip('~').strip()
    return room_info


def _parse_edesc(edesc_lines):
    edescs = {}
    key = None
    lines = None
    for line in edesc_lines:
        if 'EDESC' in line:
            keywords = line.strip('~').split()[1:]
            lines = []
            for key in keywords:
                edescs[key] = lines
        else:
            lines.append(line)

    # Make the list of description items into a single line.
    result = {key: ' '.join([line.strip('~') for line in lines])
              for key, lines in edescs.items()}

    return result


def parse_rooms(raw_room_lines):
    rooms = _get_rooms(raw_room_lines)
    result = {}
    for vnum, room_lines in rooms:
        room_dict = _get_room_dict(room_lines)
        result[vnum] = _get_room_info(room_dict)
    return result
