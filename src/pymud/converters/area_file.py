from collections import defaultdict
from typing import Dict, List
import re


def get_key_data(area_file: str) -> Dict[str, List[str]]:
    legfile = open(area_file)
    areadict = defaultdict(list)

    for line in legfile:
        stripped = line.strip()
        if not stripped:
            continue
        if re.match("^#[A-Z]{2,}", stripped):
            key = stripped.strip('#')
            continue
        areadict[key].append(stripped)
    return dict(areadict)

