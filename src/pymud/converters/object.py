import re
from typing import Dict, List


def get_obj_lines(mobdata_lines):
    mobs = {}
    for line in mobdata_lines:
        if line == '#0':
            break
        if line.startswith('#'):
            mobdata = []
            mobs[line.strip('#')] = mobdata
            continue
        mobdata.append(line)
    return mobs


def get_objects(raw_obj_data):
    objects = {}
    obj_dicts = get_obj_lines(raw_obj_data)
    for vnum, obj_dict in obj_dicts.items():
        objects[vnum] = parse_object(obj_dict)
    return objects


def parse_object(object_lines: List[str]) -> Dict[str, List[str]]:
    object_info = {}
    val = ''
    for line in object_lines:
        if line == '~':
            continue
        line = line.strip('~')
        if re.match('^[A-Z]{2,}', line):
            if val:
                object_info[key] = val
            splitline = line.split()
            if len(splitline) == 1:
                key = line
            elif len(splitline) > 1:
                key = splitline[0]
                val = ' '.join(splitline[1:])
        else:
            val += line
    return object_info
