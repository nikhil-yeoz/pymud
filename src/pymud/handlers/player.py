"""Base player connection handlers."""

import asyncio
import sys
import traceback

from pymud.races.human import Human

async def get_name(reader, writer):
    """Get player name."""
    writer.write(b'Welcome to the MUD!\n\n\rType \'help\' for help.\n\n\r')
    while True:
        await asyncio.sleep(0.1)
        writer.write(b'Please enter a name for your corpse. ')
        name = await reader.readuntil(separator=b'\n')
        break
    cleaned_name = name.strip().decode()
    print(f'His name is {cleaned_name}')
    writer.write(f'Okay cool {cleaned_name}, welcome to your end.\n\n\r'.encode())
    return cleaned_name

async def get_pass(reader, writer):
    """Get player name."""
    num_tries = 0
    while not num_tries < 3:
        await asyncio.sleep(0.1)
        writer.write(b'Password: ')
        name = await reader.readuntil(separator=b'\n')
        break
    cleaned_name = name.strip().decode()
    print(f'His name is {cleaned_name}')
    writer.write(f'Okay cool {cleaned_name}, welcome to your end.\n\n\r'.encode())
    return cleaned_name

HELP_STRING = """
quit -        quit the game.
who  -        List who's in the world.
whe -         List who's in the area you're in.
look -        Look at the room you're in.  Short command: l'
n,s,e,w,u,d - Move in the direction specified
say -         Say something to everyone in the room.
sco -         Show player score.
"""


async def player_handler(reader, writer):
    """Handle new connections."""
    name = await get_name(reader, writer)
    human = Human(name, reader, writer)
    human.world.players[human.name] = human
    await human.set_room('creation', 'beginning_of_everything')
    await human.look()
    await human.world.send_to_room(human.room, f'\u001b[32m{human.name} entered the world in a puff of smoke.\u001b[0m')
    addr = human.writer.get_extra_info('peername')
    print(f"New player connected on {addr}\n\r")
    try:
        
        # Will need to instantiate this after a login and selection

        while True:
            # Add a bit of pause so we don't go hog wild.
            await asyncio.sleep(0.1)
            data = await human.reader.readuntil(separator=b'\n')
            message = data.decode().strip()
            

            split_message = message.split()
            command = message if len(split_message) <= 1 else split_message[0]
            options = None if len(split_message) <= 1 else ' '.join(split_message[1:])
            print(f'{human.name} entered the command: {command} - {options}')

            if command == 'quit':
                break
            elif command == 'shutdown':
                sys.exit("Shutting down.\n\r")
            elif command == 'who':
                players_online = [
                    f"\u001b[37;1mCurrent players online:\u001b[0m"
                ]
                for player in human.world.players.values():
                    players_online.append(f"{player}")
                players_online.append(' ')
                await human.write('\n'.join(players_online))
            elif command == 'whe':
                players_online = [
                    f"\u001b[37;1mCurrent Players in this area:\u001b[0m"
                ]
                for player in human.world.players.values():
                    if player.area_id == human.area_id:
                        players_online.append(f"{player} - {player.room.title}")
                players_online.append(' ')
                await human.write('\n'.join(players_online))
            elif command in ('look', 'l'):
                await human.look(opt=options)
            elif command in ('n', 's', 'e', 'w', 'u', 'd'):
                direction = command
                await human.move(direction)
            elif command == 'say':
                await human.say(options)
            elif command == 'sco':
                await human.write(human.score, prompt=True)
            elif command == 'help':
                await human.write(HELP_STRING)
            elif command == 'areaupdate':
                human.world.update_areas()
                await human.write("Updated areas.")
            elif command in ('k', 'kill') and options == 'self':
                await human.lower_hp(100)
            else:
                await human.write('Huh?\n')
            await human.writer.drain()

    except Exception:
        traceback.print_exc()
    finally:
        await human.world.send_to_room(human.room, f'\u001b[32m{human.name} left the world in a violent explosion of pink mist.\u001b[0m')
        human.writer.close()
        await human.writer.wait_closed()
        print(f"Player {human} closed.")
        human.world.players.pop(human.name)
