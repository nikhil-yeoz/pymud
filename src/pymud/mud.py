"""This is the main package."""

import asyncio

from pymud.utils.run_server import run

def main():
    """Run main functions."""
    asyncio.run(run(), debug=True)

if __name__ == "__main__":
    main()

