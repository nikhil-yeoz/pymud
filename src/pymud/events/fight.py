import random
import enum
import time


fight_states = enum.IntEnum('fight_states', 'FIGHTING, DEAD, WON')


class Fight:
    round_delay = 0.01
    
    def __init__(self, fight_info, reset_after=True, start_full=True):
        self.fight_state = self.set_fight_state(fight_info)
        self._fighters = None
        self.reset_after = reset_after
        self.start_full = start_full
        
    def __repr__(self):
        return f"<Fight({self.fight_state}, reset_after={self.reset_after}, start_full={self.start_full})>"
        
    @property
    def fighters(self):
        if not self._fighters:
            fighters = set()
            for fighter_info in self.fight_state.values():
                fighters.add(fighter_info['fighter'])
                fighters.add(fighter_info['target'])
            self._fighters = fighters
        return self._fighters
        
    @property
    def fighting(self):
        """Check if anyone with a target still has a live target."""
        return any([state['state'] == fight_states.FIGHTING for state in self.fight_state.values()])
    
    @property
    def status(self):
        return [(state['fighter'], state['state']) for state in self.fight_state.values()]
        
    def set_fight_state(self, fight_info):
        fight_state = {}
        for fighter_state in fight_info:
            fighter = fighter_state['fighter']
            state = {
                'fighter': fighter,
                'target': fighter_state['target'],
                'state': fight_states.FIGHTING
            }
            fight_state[fighter.name] = state
        return fight_state
        
    def add_fighter(self, player, target):
        if player.name in self.fight_state:
            print("That person is already fighting!!")
        else:
            self.fight_state[player.name] = {
                'fighter': player,
                'target': target,
                'state': fight_states.FIGHTING
            }
        
    def get_round_order(self):
        unsorted_fight_order = []
        for fighter_name in self.fight_state:
            fighter = self.fight_state[fighter_name]['fighter']
            speed = (random.randint(80, 100) / 100) * fighter.dex + (random.randint(1, 20) / 100)
            unsorted_fight_order.append((speed, fighter_name))
        sorted_fight_order = sorted(unsorted_fight_order, key=lambda x: x[0])
        fight_ordered_players = [fighter_info[-1] for fighter_info in sorted_fight_order]
        return fight_ordered_players
    
    def process_round(self):
        round_order = self.get_round_order()
        for player_name in round_order:
            player = self.fight_state[player_name]['fighter']
            target = self.fight_state[player.name]['target']
            hp_hit = random.randint(0, 10) * player.str
            target.current_hp -= hp_hit
            print(f"{player.name} hits {target.name} for {hp_hit} hp! He has {target.current_hp} left!")
            
            DB_CONFIG.SESSION.commit()
            if target.current_hp <= 0:
                self.fight_state[target.name]['state'] = fight_states.DEAD
                self.fight_state[player.name]['state'] = fight_states.WON
                print(f"{target.name} has been killed!")
                break
    
    def process_fight(self):
        if self.start_full:
            self.reset_fighters()
        while self.fighting:
            self.process_round()
            time.sleep(self.round_delay)
        if self.reset_after:
            self.reset_fighters()
        print(f"The winners are : {', '.join([fighter_name for fighter_name, fighter_state in self.fight_state.items() if fighter_state['state'] == fight_states.WON])}")
            
    def reset_fighters(self):
        for player in self.fighters:
            player.current_hp = player.base_hp
            print(f"Player {player.name} has been restored!")