"""Human specific race implementations."""

from pymud.player.player_classes import Player

class Human(Player):
    """Base human class."""

    def __init__(self, name, reader, writer):
        """Create a human instance."""
        super().__init__(name, reader, writer)
