from sqlalchemy import Column, Integer, Boolean, ForeignKey, String
from sqlalchemy_utils import ChoiceType, JSONType

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship

from pymud.config.database import DB_CONFIG
from pymud.events.fight import Fight


Base = declarative_base()

INVERSE_DIRECTIONS = {
    'north': 'south',
    'south': 'north',
    'east': 'west',
    'west': 'east',
    'up': 'down',
    'down': 'up',
}


class TableBase(object):

    @declared_attr
    def __tablename__(cls):  # pylint: disable=no-self-argument
        return cls.__name__.lower()  # pylint: disable=no-member

    id = Column(Integer, primary_key=True)


class Stats(object):
    str = Column(Integer, default=1)
    dex = Column(Integer, default=1)
    con = Column(Integer, default=1)
    int = Column(Integer, default=1)
    wis = Column(Integer, default=1)
    cha = Column(Integer, default=1)


class Exit(Base, TableBase):

    DIRECTIONS = [
        (u'north', u'North'),
        (u'south', u'South'),
        (u'east', u'East'),
        (u'west', u'West'),
        (u'up', u'Up'),
        (u'down', u'Down'),
    ]

    direction = Column(ChoiceType(DIRECTIONS), nullable=False)
    is_locked = Column(Boolean, default=False)
    is_open = Column(Boolean, default=True)
    is_closeable = Column(Boolean, default=False)
    description = Column(String)
    target_room = ForeignKey('Room', nullable=False)
    room_id = Column(Integer, ForeignKey('room.id'))

    room =  relationship("Room", back_populates='exits')

    def __str__(self):
        if self.is_open:
            return_string = f"{self.direction.value}"
        else:
            return_string = f"[{self.direction.value}]"
        return return_string

    def __repr__(self):
        return f"<Exit(direction={self.direction}, target_room={self.target_room.name})>"


class Room(Base, TableBase):

    name = Column(String, nullable=False)
    description = Column(String, nullable=False)
    extras = Column(JSONType)

    exits =  relationship('Exit', back_populates='room', lazy='dynamic')
    mobs =  relationship('Mob', back_populates='room', lazy='dynamic')
    players =  relationship('Player', back_populates='room', lazy='dynamic')
    items =  relationship('Item', back_populates='room', lazy='dynamic')

    def __repr__(self):
        return f"<Room(name={self.name}, description={self.description})>"

    def is_bidirectional(self, direction):
        is_bidirectional=False
        return_room = self.get_return_room(direction)
        if self.id == return_room.id:
            is_bidirectional = True
        return is_bidirectional

    def get_return_exit(self, direction):
        exits = {ex.direction.code: ex for ex in self.exits}
        target_room = exits[direction].target_room
        return_direction = INVERSE_DIRECTIONS[direction]
        target_room_exits = {ex.direction.code: ex for ex in target_room.exits}
        return_exit = target_room_exits[return_direction]
        return return_exit

    def get_return_room(self, direction):
        exits = {ex.direction.code: ex for ex in self.exits}
        target_room = exits[direction].target_room
        return_direction = INVERSE_DIRECTIONS[direction]
        target_room_exits = {ex.direction.code: ex for ex in target_room.exits}
        return_room = target_room_exits[return_direction].target_room
        return return_room


class Player(Base, TableBase, Stats):

    name = Column(String)
    password = Column(String)
    title = Column(String, default='Noob')
    description = Column(String)
    base_hp = Column(Integer, default=100)
    base_mp = Column(Integer, default=100)
    base_mv = Column(Integer, default=100)
    current_hp = Column(Integer, default=100)
    current_mp = Column(Integer, default=100)
    current_mv = Column(Integer, default=100)
    level = Column(Integer, default=1)
    xp = Column(Integer, default=0)
    room =  relationship("Room", back_populates='players')
    room_id = Column( Integer,  ForeignKey('room.id'))
    items =  relationship('Item', back_populates='player', lazy='dynamic')
    equipped = Column(JSONType)

    def __repr__(self):
        return f"<Player(name='{self.name}', short='{self.name} the {self.title}')>"

    def _look_room(self):
        look_lines = [
            self.room.name,
            '',
            self.room.description,
            '',
            '[Exits: ' + ' '.join(sorted([str(eobj) for eobj in self.room.exits.all()])) + ']',
            '',
        ]

        for player in self.room.players.filter(Player.id != self.id).all():
            look_lines.append(f"{player.name} is here")
        look_lines.append('')
        for mob in self.room.mobs:
            look_lines.append(mob.long)
        for item in self.room.items:
            look_lines.append(item.long)

        print('\n'.join(look_lines))

    def _look_target(self, target):
        matches = []
        if target == self.name:
            print("You look at yourself and go cross eyed.")
            return
        for player in self.room.players.filter(Player.id != self.id).filter(Player.name == target):
            matches.append(player.description)
        for mob in self.room.mobs.filter(Mob.name.ilike(f'%{target}%')):
            matches.append(mob.description)
        for item in self.room.items.filter(Item.name.ilike(f'%{target}%')):
            matches.append(item.description)
        for ex in self.room.exits.filter(Exit.description.ilike(f'%{target}%')):
            matches.append(ex.description)
        if not matches:
            print("Can't seem to find that anywhere.")
        elif len(matches) == 1:
            print(matches[0])
        else:
            print(f"Multiple things found for {target}:")
            for match in matches:
                print(match)

    def show_inventory(self):
        print("You are carrying:\n")
        items = self.items.all()
        if not items:
            print('Nothing')
        else:
            for item in items:
                print(item.short)

    def kill(self, target_name):
        matches = []
        for target in self.room.players.filter(Player.id != self.id).filter(Player.name == target_name):
            matches.append(target)
        for target in self.room.mobs.filter(Mob.name.ilike(f'%{target_name}%')):
            matches.append(target)
        if len(matches) > 1:
            print("Too many options to choose from.")
        elif len(matches) == 0:
            print("Nothing to kill around here like that.")
        else:
            target = matches[0]
        if target is not None:
            fight_info = [
                {'fighter': self, 'target': target},
                {'fighter': target, 'target': self},
            ]
            print(f"{self.name} is going to fight {target.name}")
            fight = Fight(fight_info)
            fight.process_fight()
        DB_CONFIG.SESSION.commit()
        return fight

    def look(self, target=None):
        if target is not None:
            self._look_target(target)
        else:
            self._look_room()

    def move(self, direction, silent=False):
        exits = {ex.direction.code: ex for ex in self.room.exits}
        target_exit = exits.get(direction)
        if target_exit and target_exit.is_open:
            target_room = target_exit.target_room
            self.room = target_room
            DB_CONFIG.SESSION.commit()
            if silent:
                return
            print(f"You move {direction}.")
            self.look()
        else:
            print("Can't go that way")

    def open(self, direction, silent=False):
        exits = {ex.direction.code: ex for ex in self.room.exits}
        target_exit = exits.get(direction)
        if not target_exit:
            print("Nothing to open that direction!")
        elif not target_exit.is_closeable:
            print("You can't open that!")
        elif target_exit.is_open:
            print("It's already opened!")
        elif target_exit.is_locked:
            print("It's locked!")
        else:
            target_exit.is_open = True
            if self.room.is_bidirectional(direction):
                return_exit = self.room.get_return_exit(direction)
                return_exit.is_open = True
            DB_CONFIG.SESSION.commit()
            if silent:
                return
            print(f"You managed to open it.")

    def close(self, direction, silent=False):
        exits = {ex.direction.code: ex for ex in self.room.exits}
        target_exit = exits.get(direction)
        if not target_exit:
            print("Nothing to close that direction!")
        elif not target_exit.is_closeable:
            print("You can't close that!")
        elif not target_exit.is_open:
            print("It's already closed!")
        else:
            target_exit.is_open = False
            if self.room.is_bidirectional(direction):
                return_exit = self.room.get_return_exit(direction)
                return_exit.is_open = False
            DB_CONFIG.SESSION.commit()
            if silent:
                return
            print(f"You managed to close it.")

    def get(self, target_name, silent=False):
        matches = []
        target = None
        for target in self.room.items.filter(Item.name.ilike(f'%{target_name}%')):
            matches.append(target)
        if len(matches) > 1:
            print("That could be so many things...")
        elif len(matches) == 0:
            print(f"You don't see any {target_name} here.")
        else:
            target = matches[0]
        if target is not None:
            target.player = self
            target.room = None
            DB_CONFIG.SESSION.commit()
            if silent:
                return
            print(f"You get a {target.name}")

    def drop(self, target_name, silent=False):
        matches = []
        target = None
        for target in self.items.filter(Item.name.ilike(f'%{target_name}%')):
            matches.append(target)
        if len(matches) > 1:
            print("That could be so many things...")
        elif len(matches) == 0:
            print(f"You don't see any {target_name} in your inventory.")
        else:
            target = matches[0]
        if target is not None:
            target.player = None
            target.room = self.room
            DB_CONFIG.SESSION.commit()
            if silent:
                return
            print(f"You drop a {target.name}")


    def give(self, item, target):
        item_iobj = None
        target_pobj = None

        possible_items = self.items.filter(Item.name.contains(item)).all()
        if len(possible_items) > 1:
            print("But you have more than one of that!")
        elif not possible_items:
            print("You don't have any of that to give!")
        else:
            item_iobj = possible_items[0]
        possible_targets = DB_CONFIG.SESSION.query(Player).filter(Player.name.like(target)).filter(Player.name != self.name).all()
        if len(possible_targets) > 1:
            print("You're seeing double and can't decide which one to give it to!")
        elif not possible_targets:
            print("You don't see anyone here like that to give it to.")
        else:
            target_pobj = possible_targets[0]

        if item_iobj and target_pobj:
            item_iobj.player = target_pobj
            print(f"{self.name} gives {item} to {target_pobj.name}")
            DB_CONFIG.SESSION.commit()


class Mob(Base, TableBase, Stats):

    name = Column(String)
    short = Column(String)
    long = Column(String)
    description = Column(String)
    base_hp = Column( Integer)
    base_mp = Column( Integer)
    base_mv = Column( Integer)
    current_mp = Column( Integer)
    current_hp = Column( Integer)
    current_mv = Column( Integer)
    level = Column( Integer)
    room =  relationship("Room", back_populates='mobs')
    room_id = Column( Integer,  ForeignKey('room.id'))
    items =  relationship('Item', back_populates='mob', lazy='dynamic')

    def __repr__(self):
        return f"<Mob(name='{self.name}', short='{self.short}')>"


class Item(Base, TableBase):
    ITEM_TYPES = [
        ('weapon', 'Weapon'),
        ('consumable', 'Consumable'),
        ('key', 'Key'),
        ('armor', 'Armor'),
        ('currency', 'Currency'),
    ]

    name = Column(String)
    short = Column(String)
    long = Column(String)
    description = Column(String)
    room =  relationship("Room", back_populates='items')
    room_id = Column( Integer,  ForeignKey('room.id'))
    player = relationship("Player", back_populates='items')
    player_id = Column(Integer, ForeignKey('player.id'))
    mob = relationship("Mob", back_populates='items')
    mob_id = Column(Integer, ForeignKey('mob.id'))
    item_type = Column(ChoiceType(ITEM_TYPES), nullable=False, default='armor')
    extras = Column(JSONType)

    def __repr__(self):
        return f"<Item(name='{self.name}', short='{self.short}')>"
