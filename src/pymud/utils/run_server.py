"""This is the main package."""

import asyncio

from pymud.handlers.player import player_handler
from pymud.exceptions import KillServer

async def run() -> None:
    """Run main mud server."""
    server = await asyncio.start_server(player_handler, '0.0.0.0', 4449)

    try:
        addr = server.sockets[0].getsockname()
        print(f'Serving on {addr}')
        await server.serve_forever()
    except (KeyboardInterrupt, KillServer) as exc:
        server.close()
        print(f'Shutting down server: {exc}')