"""Utilities for modifying output."""

from typing import List

def split_by_leng(text: str, width: int = 80) -> str:
    """Split a long text into a list of strings by width."""
    lines = []
    working_line = ''
    words = text.split()
    for i in range(len(words)):
        new_word = words[i]
        # Check if the line plus a space and the new word are greater than the terminal
        # width.
        if len(working_line) + len(new_word) + 1 > width:
            lines.append(working_line.strip())
            working_line = new_word
        else:
            working_line += f' {new_word}'
    if working_line:
        lines.append(working_line.strip())

    return '\n'.join(lines)
