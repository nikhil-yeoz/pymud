import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


class DBConfig:
    """Class instance to make sure that we're giving back the same config objects."""

    # URL that we'll use to connect to the database.
    PYMUD_DB_URL = os.environ.get('PYMUD_DB_URL') or 'sqlite:///tutorial.db'

    def __init__(self):
        """Empty initialization for config."""
        self._engine = None
        self._session = None

    @property
    def ENGINE(self):
        if not self._engine:
            self._engine = create_engine(self.PYMUD_DB_URL, echo=False)
        return self._engine

    @property
    def SESSION(self):
        if not self._session:
            Session = sessionmaker(bind=self.ENGINE)
            self._session = Session()
        return self._session

DB_CONFIG = DBConfig()