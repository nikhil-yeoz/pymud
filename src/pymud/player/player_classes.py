"""Base race class."""

import codecs
import random

from pymud.utils.score import format_score
from pymud.utils.output_utils import split_by_leng



class Player:
    """Base race class for others to inherit from."""

    def __init__(self, name, reader, writer):
        """Set initial stats of race instance."""
        self.name = name
        self.title = "the Rando"
        self.base_hp = 100
        self.base_mp = 100
        self.base_mv = 100
        self.current_hp = 100
        self.current_mp = 100
        self.current_mv = 100
        self.base_xp = 0
        self.reader = reader
        self.writer = writer
        self.room_id = None
        self.area_id = None

    def __str__(self):
        """Return name of player as string."""
        return self.name

    def __repr__(self):
        """Return name of player as repr."""
        return f"Player: {self.name}"

    async def write(self, message: str, prompt=False) -> None:
        """Write a message to a user."""
        for line in message.splitlines():
            encoded = codecs.encode(line + '\n\r')
            self.writer.write(encoded)
        prompt_with_lines = '\n' + self.prompt + '\n\r'
        if prompt:
            encoded_prompt = codecs.encode(
                f"\u001b[37m{prompt_with_lines}\u001b[0m"
            )
            self.writer.write(encoded_prompt)
        await self.writer.drain()

    @property
    def score(self):
        """Return information about the player."""
        return format_score(self)

    @property
    def prompt(self):
        """Return a string for user prompt."""
        prompt_str = f'<{self.current_hp}/{self.base_hp}hp {self.current_mp}/{self.base_mp}mp {self.current_mv}/{self.base_mv}mv {self.base_xp}>'
        return prompt_str

    @property
    def room(self):
        return self.world.areas[self.area_id].rooms[self.room_id]

    async def set_room(self, area_id, room_id):
        """Set players current room."""
        self.room_id = room_id
        self.area_id = area_id

    async def look(self, opt=None):
        """Print what the user would see."""
        players_in_room = self.world.get_players_in_room(self.room)
        other_players = [
            player for player in players_in_room if player != self]

        if opt is not None:
            if opt in [player.name for player in players_in_room]:
                view = "One day you'll be able to look at other players, but not yet."
            elif opt in self.room.viewables:
                raw_view = self.room.viewables.get(opt)
                view = split_by_leng(raw_view)
            else:
                view = "You do not see that here."
        else:
            player_view_strings = [
                f"{player.name.title()} is here.\n\r" for player in other_players]
            pvs = ''.join(player_view_strings)
            view = '\n\n'.join([str(self.room), pvs])
        await self.write(view, prompt=True)

    async def move(self, direction_raw: str) -> None:
        """Move the player."""
        if direction_raw == 'n':
            direction = 'north'
        elif direction_raw == 's':
            direction = 'south'
        elif direction_raw == 'e':
            direction = 'east'
        elif direction_raw == 'w':
            direction = 'west'
        elif direction_raw == 'u':
            direction = 'up'
        elif direction_raw == 'd':
            direction = 'down'
        else:
            direction = direction_raw
        if direction in self.room.exits:
            # Send leave message
            players_in_room = self.world.get_players_in_room(self.room)
            players_to_message = [
                player for player in players_in_room if player != self]
            await self.world.send_to_players(players_to_message, f'{self.name.title()} has left {direction}.')
            # Set the current room
            print(self.room.exits)
            current_room = self.world.areas[self.area_id].rooms[self.room_id]
            print(f'Room exits: {current_room.exits}')
            print(f"Trying to move in direction {direction}")
            new_room_info = current_room.exits[direction]
            if isinstance(new_room_info, dict):
                print(new_room_info)
                self.area_id = new_room_info['area']
                self.room_id = new_room_info['room']
            else:
                self.room_id = new_room_info
            print(f"New room id: {self.room_id}")
            # Send entry message
            players_in_room = self.world.get_players_in_room(self.room)
            players_to_message = [
                player for player in players_in_room if player != self]
            await self.world.send_to_players(players_to_message, f'{self.name.title()} has arrived.')
            await self.look()
        else:
            text = "You can't go that way."
            await self.write(text)

    async def say(self, message: str) -> None:
        """Send a message to the room."""
        players_in_room = self.world.get_players_in_room(self.room)
        players_to_message = [
            player for player in players_in_room if player != self]
        your_message = f"\u001b[1mYou say \"{message}\"\u001b[0m"
        their_message = f"\u001b[1m{self.name.title()} says '{message}'\u001b[0m"
        await self.write(your_message)
        await self.world.send_to_players(players_to_message, their_message)

    async def check_status(self):
        """Check status and die, otherwise show result."""
        if self.current_hp <= 0:
            await self.write("YOU HAVE DIED!!!")
            self.current_hp = 100
            self.current_mp = 100
            self.current_mv = 100

    async def lower_hp(self, amount: int) -> None:
        """Lower HP of player."""
        self.current_hp -= amount
        self.check_status()

    async def lower_mp(self, amount: int) -> None:
        """Lower Mana of player."""
        self.current_mp -= amount
        self.check_status()

    async def lower_mv(self, amount: int) -> None:
        """Lower Moves of player."""
        self.current_mv -= amount
        self.check_status()
