# pymud

A python mud server

Installation should be simple:

`git clone https://gitlab.com/bubthegreat/pymud.git`
`cd pymud`
`pip install .`

Alternatively:

`pip install https://gitlab.com/bubthegreat/pymud.git`

After installation, you should have a command: `pymud`.

```
bub@LappyToppy:/mnt/c/Users/bubth/Development/pymud$ pymud
Getting area files.
area files:  ['/home/bub/.local/lib/python3.7/site-packages/pymud/areas/creation.yml', '/home/bub/.local/lib/python3.7/site-packages/pymud/areas/desolation.yml']
Got areas.
Serving on ('0.0.0.0', 4449)
```

Connect with your telnet client of choice.  I use telnet when I'm feeling lazy with `telnet localhost 4449`

You should see a welcome screen:
```
Welcome to the MUD!

Type 'help' for help.

Please enter a name for your corpse. bub
Okay cool bub, welcome to your end.

The Beginning of Everything

This strange place is in the center of a myriad of environments. To the north
you see a pathway through a garden full of statues on either side of the path.
The pathway circles back to where you stand. To the east, you see a strange
nebula of dark crackling energy. To the west, a forest spans as wide as you can
see. To the south, the edge of the forest butts up against a small glass
building that appears to be a library.

[Exits: north south east west]


<100/100hp 100/100mp 100/100mv 0>
bub entered the world in a puff of smoke.
```

Type `help` for a list of commands available in the mud, and for the love of all that's holy, contribute if you want to!