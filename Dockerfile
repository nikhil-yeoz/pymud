FROM python:3.7-alpine

RUN pip install pyyaml
WORKDIR /pymud
COPY . .

RUN pip install .

EXPOSE 8000

CMD pymud