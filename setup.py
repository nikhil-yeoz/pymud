"""Setup configuration and dependencies for pymud."""

import codecs
import os
import setuptools
import re

here = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    """Join parts of the file path parts and read them."""
    with codecs.open(os.path.join(here, *parts), 'r') as fp:
        return fp.read()


def find_version(*file_paths):
    """Read the file path for a version string."""
    version_file = read(*file_paths)
    version_match = re.search(
        r"^__version__\s?=\s?['\"]([^'\"]*)['\"]",
        version_file,
        re.M,
    )
    if version_match:
        return version_match.group(1)

    raise RuntimeError("Unable to find version string.")


COMMANDS = [
    'pymud=pymud.mud:main',
]

PACKAGES = setuptools.find_packages('src')

REQUIREMENTS = REQUIREMENTS = [requirement for requirement in open('requirements.txt').readlines()]

setuptools.setup(
    name='pymud',
    version=find_version("src", "pymud", "__init__.py"),
    description='This is a fancy python based mud!',
    packages=PACKAGES,
    package_dir={'': 'src'},
    include_package_data=True,
    python_requires='>=3.6',
    entry_points={'console_scripts': COMMANDS},
    install_requires=REQUIREMENTS,
    url='https://gitlab.com/bubthegreat/pymud',
)
