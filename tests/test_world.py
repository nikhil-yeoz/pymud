import os

import pytest
from pymud.builders.world import get_area_files
from pymud.builders.world import get_raw_areas
from pymud.builders.world import get_areas

AREAS = [
    'creation',
    'desolation'
]

def test_all_files_present():
    """Check that we get all the files for the areas we currently have."""
    expected = [f'{area}.yml' for area in AREAS]
    result = [os.path.basename(file) for file in get_area_files()]

    assert expected == result


def test_all_keys_present():
    """Check that we parsed all the areas from the area files."""
    expected = AREAS
    area_files = get_area_files()
    raw_areas = get_raw_areas(area_files)
    result = list(raw_areas.keys())

    assert expected == result

def test_get_areas():
    """Test that we get area objects we expect."""
    expected = AREAS
    areas = get_areas()
    result = list(areas.keys())

    assert expected == result
