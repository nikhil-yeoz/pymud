import pytest

from pymud.utils.score import format_score
from pymud.player.player_classes import Player


class FakePlayer:
    def __init__(self):
        self.name = 'bub'
        self.title = "Rando"
        self.base_hp = 100
        self.base_mp = 100
        self.base_mv = 100
        self.current_hp = 100
        self.current_mp = 100
        self.current_mv = 100
        self.base_xp = 0

@pytest.fixture
def player(scope='module'):
    player = FakePlayer()
    return player

def test_score(player):
    """Test that we return a correct score from a player."""

    expected = [
        '',
        '\u001b[37;1mbub the Rando.\u001b[0m',
        '--------------------------------',
        'hitpoints: 100/100',
        'mana:      100/100',
        'moves:     100/100',
        '--------------------------------'
    ]
    result = format_score(player).splitlines()
    assert result == expected
